/* (c) 2013 Paul Morris */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */
// JavaScript Document
var AVR = (function () {
  "use strict";
  return {
    // this playNote method is overwritten by loadAudio
    // if loadAudio is not called then there is no audio support
    // in that case playNote shows this alert once and then self-destructs
    playNote: function () {
      alert(
        "TwinNote.org says: your browser does not support HTML5 audio," +
          " so there will be no sound. The visuals should still work. " +
          "To hear the audio, please use the most recent version of a good" +
          "standards-compliant browser like Mozilla Firefox."
      );
      AVR.playNote = function () {};
    },

    // Volume Control
    // overwritten in IE9
    changeVolume: function (newLevel, newLabel) {
      document.getElementById("volumeControl").innerHTML =
        "Volume: " + newLabel;
      if (newLevel > 0) {
        Howler.unmute();
        Howler.volume(newLevel);
      } else {
        Howler.mute();
      }
    },

    piano: null,

    loadUI: function () {
      if (document.getElementById("playButton")) {
        document.getElementById("playButton").innerHTML =
          '<a href="javascript:" onclick="AVR.playback()" class="controlbutton">Play Audio</a>';
      }
      if (document.getElementById("keyboardControl")) {
        document.getElementById("keyboardControl").style.display = "block";
      }
      // for game
      if (document.getElementById("feedback")) {
        document.getElementById("feedback").innerHTML = "&nbsp;";
      }
      // for both game and audiovisualizer
      if (document.getElementById("volumeControl")) {
        document.getElementById("volumeControl").style.display = "block";
      }
      // this function will self destruct!
      AVR.loadUI = function () {};
    },

    // howler.js
    loadAudio: function () {
      var a;
      // volume level 2
      Howler.volume(0.35);

      if (document.documentElement.id !== "ie9") {
        AVR.piano = new Howl({
          urls: [
            "https://twinnote.clairnote.org/wp/wp-content/themes/twinnote/audio/sprites-3octaves-3secs-mono.mp3",
            "https://twinnote.clairnote.org/wp/wp-content/themes/twinnote/audio/sprites-3octaves-3secs-mono.ogg",
          ],
          autoplay: false,
          onload: function () {
            AVR.loadUI();
          },
          sprite: {
            0: [0, 1500], // F 33
            1: [3000, 1500],
            2: [6000, 1500], // G 35
            3: [9000, 1500],
            4: [12000, 1500], // A 37
            5: [15000, 1500],
            6: [18000, 1500], // B 39
            7: [21000, 1500], // C 40
            8: [24000, 1500],
            9: [27000, 1500], // D 42
            10: [30000, 1500],
            11: [33000, 1500], // E 44
            12: [36000, 1500], // F 45
            13: [39000, 1500],
            14: [42000, 1500], // G 47
            15: [45000, 1500],
            16: [48000, 1500], // A 49
            17: [51000, 1500],
            18: [54000, 1500], // B 51
            19: [57000, 1500], // C 52
            20: [60000, 1500],
            21: [63000, 1500], // D 54
            22: [66000, 1500],
            23: [69000, 1500], // E 56
            24: [72000, 1500], // F 57
            25: [75000, 1500],
            26: [78000, 1500], // G 59
            27: [81000, 1500],
            28: [84000, 1500], // A 61
            29: [87000, 1500],
            30: [90000, 1500], // B 63
            31: [93000, 1500], // C 64
            32: [96000, 1500],
            33: [99000, 1500], // D 66
            34: [102000, 1500],
            35: [105000, 1500], // E 68
            36: [108000, 1500], // F 69
          },
        });

        AVR.playNote = function (n) {
          AVR.piano.play(n.toString());
        };
      } else {
        // for IE9
        AVR.piano = [];
        AVR.piano[36] = [];
        AVR.piano[36][0] = new Audio();
        for (a = 0; a < 37; a += 1) {
          AVR.piano[a] = [];
          AVR.piano[a][0] = new Audio(
            "https://twinnote.clairnote.org/wp/audio-piano-1/" +
              (a + 33) +
              ".mp3"
          );
          // fix IE9 so it will cloneNode, still doesn't work most of the time
          document.body.appendChild(AVR.piano[a][0]);
          AVR.piano[a][0].load();
          AVR.piano[a][0].volume = 0.65;
        }
        AVR.piano[36][0].addEventListener(
          "canplaythrough",
          AVR.loadUI(),
          false
        );

        // playNote function for IE9
        AVR.chan = 0;
        AVR.prevNote = null;
        AVR.playNote = function (n) {
          if (n !== AVR.prevNote) {
            AVR.chan = 0;
            AVR.prevNote = n;
          }

          // originally added for IE9 before howler
          try {
            AVR.piano[n][AVR.chan].currentTime = 0;
          } catch (e) {
            AVR.chan = 0;
            AVR.piano[n][AVR.chan].currentTime = 0;
          }
          try {
            AVR.piano[n][AVR.chan].play();
          } catch (ignore) {}

          AVR.chan += 1;
          if (AVR.chan >= 3) {
            AVR.chan = 0;
          }

          if (AVR.piano[n][AVR.chan] === undefined) {
            AVR.piano[n][AVR.chan] = AVR.piano[n][0].cloneNode(true);
            AVR.piano[n][AVR.chan].load();
            AVR.piano[n][AVR.chan].volume = 0.65;
          }
        };
        // changeVolume for IE9
        AVR.volumeLevel = 0.65;
        AVR.changeVolume = function (newLevel, newLabel) {
          var i;
          AVR.volumeLevel = newLevel;
          document.getElementById("volumeControl").innerHTML =
            "Volume: " + newLabel;
          for (i = 0; i < 37; i += 1) {
            try {
              AVR.piano[i][0].volume = AVR.volumeLevel;
            } catch (ignore) {}
            try {
              AVR.piano[i][1].volume = AVR.volumeLevel;
            } catch (ignore) {}
            try {
              AVR.piano[i][2].volume = AVR.volumeLevel;
            } catch (ignore) {}
          }
        };
      }
    },

    // Check for audio support
    audioCheck: function () {
      var audioTest = document.createElement("audio");
      try {
        if (!!audioTest.canPlayType) {
          AVR.loadAudio();
        } // else piano === null
      } catch (ignore) {}
      audioTest = null;
    },

    // Note Patterns (horizontal background)

    twinNoteNotes: [
      0, -750, 0, -750, 0, -750, 0, -750, 0, -750, 0, -750, 0, -750, 0, -750, 0,
      -750, 0, -750, 0, -750, 0, -750, 0, -750, 0, -750, 0, -750, 0, -750, 0,
      -750, 0, -750, 0,
    ],

    allHollowNotes: [
      -250, -750, -250, -750, -250, -750, -250, -750, -250, -750, -250, -750,
      -250, -750, -250, -750, -250, -750, -250, -750, -250, -750, -250, -750,
      -250, -750, -250, -750, -250, -750, -250, -750, -250, -750, -250, -750,
      -250,
    ],

    allSolidNotes: [
      0, -500, 0, -500, 0, -500, 0, -500, 0, -500, 0, -500, 0, -500, 0, -500, 0,
      -500, 0, -500, 0, -500, 0, -500, 0, -500, 0, -500, 0, -500, 0, -500, 0,
      -500, 0, -500, 0,
    ],

    // Current defaults

    currentNotePositions: [
      63, 63, 56, 56, 49, 49, 42, 42, 35, 35, 28, 28, 21, 21, 14, 14, 7, 7, 0,
      0, -7, -7, -14, -14, -21, -21, -28, -28, -35, -35, -42, -42, -49, -49,
      -56, -56, -63,
    ],

    currentNotePattern: [
      0, -750, 0, -750, 0, -750, 0, -750, 0, -750, 0, -750, 0, -750, 0, -750, 0,
      -750, 0, -750, 0, -750, 0, -750, 0, -750, 0, -750, 0, -750, 0, -750, 0,
      -750, 0, -750, 0,
    ], // HACKED OUT AVR.twinNoteNotes.slice(),

    currentStaffPosition: -105,

    currentLedgerPattern: [
      -1150, -1100, -1100, -1100, -1100, -1050, -1050, -1050, -1000, -1000,
      -1000, -1000, -1000, -1000, -1000, -1000, -1000, -1050, -1050, -1050,
      -1000, -1000, -1000, -1000, -1000, -1000, -1000, -1000, -1000, -1050,
      -1050, -1050, -1050, -1100, -1100, -1100, -1100, -1150, -1150, -1150,
      -1150, -1200, -1200, -1200, -1200, -1250, -1250, -1250, -1250,
    ],

    currentLedgerPositions: [
      42, 42, 42, 42, 42, 42, 42, 42, 42, -7, -7, -14, -21, -21, -21, -28, -35,
      0, 0, 0, 0, -49, -49, -56, -63, -63, -63, -189, -77, -42, -42, -42, -42,
      -56, -56, -56, -56, -70, -70, -70, -70, -84, -84, -84, -84, -98, -98, -98,
      -98,
    ],

    currentBothStemSidesInterval: 4,

    noteCaptions: [
      "F<br>&nbsp;",
      "F#<br>Gb",
      "G<br>&nbsp;",
      "G#<br>Ab",
      "A<br>&nbsp;",
      "A#<br>Bb",
      "B<br>&nbsp;",
      "C<br>&nbsp;",
      "C#<br>Db",
      "D<br>&nbsp;",
      "D#<br>Eb",
      "E<br>&nbsp;",
      "F<br>&nbsp;",
      "F#<br>Gb",
      "G<br>&nbsp;",
      "G#<br>Ab",
      "A<br>&nbsp;",
      "A#<br>Bb",
      "B<br>&nbsp;",
      "C<br>&nbsp;",
      "C#<br>Db",
      "D<br>&nbsp;",
      "D#<br>Eb",
      "E<br>&nbsp;",
      "F<br>&nbsp;",
      "F#<br>Gb",
      "G<br>&nbsp;",
      "G#<br>Ab",
      "A<br>&nbsp;",
      "A#<br>Bb",
      "B<br>&nbsp;",
      "C<br>&nbsp;",
      "C#<br>Db",
      "D<br>&nbsp;",
      "D#<br>Eb",
      "E<br>&nbsp;",
      "F<br>&nbsp;",
    ],

    intervalCaptions: [
      0,
      "Minor<br>2nd",
      "Major<br>2nd",
      "Minor<br>3rd",
      "Major<br>3rd",
      "Perfect<br>4th",
      "Tritone<br>&nbsp;",
      "Perfect<br>5th",
      "Minor<br>6th",
      "Major<br>6th",
      "Minor<br>7th",
      "Major<br>7th",
      "Octave<br>&nbsp;",
    ],

    // default is chromatic scale
    noteBank: [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19],

    // Play Back Count (pbc)
    // keeps track of which note we're on during playback(),
    // needed because passing parameters via setTimeout is non-trivial
    pbc: 0,
    whatNoteMargin: null,

    // default for initial chromatic scale
    noteMargins: [17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17],
    currentNoteCaptions: [
      "F<br>&nbsp;",
      "F#<br>Gb",
      "G<br>&nbsp;",
      "G#<br>Ab",
      "A<br>&nbsp;",
      "A#<br>Bb",
      "B<br>&nbsp;",
      "C<br>&nbsp;",
      "C#<br>Db",
      "D<br>&nbsp;",
      "D#<br>Eb",
      "E<br>&nbsp;",
      "F<br>&nbsp;",
      "F#<br>Gb",
      "G<br>&nbsp;",
      "G#<br>Ab",
      "A<br>&nbsp;",
      "A#<br>Bb",
      "B<br>&nbsp;",
      "C<br>&nbsp;",
      "C#<br>Db",
      "D<br>&nbsp;",
      "D#<br>Eb",
      "E<br>&nbsp;",
      "F<br>&nbsp;",
      "F#<br>Gb",
      "G<br>&nbsp;",
      "G#<br>Ab",
      "A<br>&nbsp;",
      "A#<br>Bb",
      "B<br>&nbsp;",
      "C<br>&nbsp;",
      "C#<br>Db",
      "D<br>&nbsp;",
      "D#<br>Eb",
      "E<br>&nbsp;",
      "F<br>&nbsp;",
    ], // HACKED OUT: AVR.noteCaptions.slice(),

    // the middle position on the current staff where stems switch from up to down.
    stemFlip: 18,

    currentInterval: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    sfri: false,

    delayBank: [
      500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500,
    ], // default half seconds
    thisTimestamp: null,
    lastTimestamp: null,
    noteCount: 0,
    needsReset: true, // does the staff need to be cleared first or not?
    pbtimeout: null, // for playLoop timer

    // note is the note, ntrvl is the interval, mrgn is the margin

    // User Interface Functions
    playBackOn: true,

    // with support for two staves for game
    clearStaff: function (staff) {
      if (staff === 2) {
        document.getElementById("note_series2").innerHTML = "";
      } else {
        document.getElementById("note_series1").innerHTML = "";
      }
    },

    togglePlayButton: function (pl) {
      switch (pl) {
        case 0:
          AVR.playBackOn = false;
          document.getElementById("playButton").innerHTML =
            '<a href="javascript:" onclick="AVR.playback()" class="controlbutton">Play Audio</a>';
          document.getElementById("note_series1").style.overflow = "auto";
          break;
        case 1:
          AVR.playBackOn = true;
          document.getElementById("playButton").innerHTML =
            '<a href="javascript:" onclick="AVR.stopPlayback()" class="controlbutton">Stop Audio</a>';
          break;
      }
    },

    stopPlayback: function () {
      // stops any playback, and resets the play button
      AVR.togglePlayButton(0);
      AVR.clearStaff();
      // re-load the notes on the staff
      AVR.drawNoteSeries();
    },

    // OTHER UI

    resetAll: function () {
      AVR.clearStaff();
      AVR.noteBank = [];
      AVR.delayBank = [];
      AVR.noteCount = 0;
      // stop the playback if it is on, and reset the play button
      AVR.togglePlayButton(0);
      // document.getElementById('notes_title').innerHTML = "&nbsp;";
    },

    instrument_toggle: function (whatKeys) {
      switch (whatKeys) {
        case 0:
          // Instrument layouts...
          document.getElementById("about66").style.display = "none";
          break;
        case 1:
          // Piano
          document.getElementById("keys_1row").style.display = "none";
          document.getElementById("keys_sixsix").style.display = "none";
          document.getElementById("keys_piano").style.display = "block";
          break;
        case 2:
          // String
          document.getElementById("keys_sixsix").style.display = "none";
          document.getElementById("keys_piano").style.display = "none";
          document.getElementById("keys_1row").style.display = "block";
          break;
        case 3:
          // 6-6
          document.getElementById("keys_1row").style.display = "none";
          document.getElementById("keys_piano").style.display = "none";
          document.getElementById("keys_sixsix").style.display = "block";
          break;
      }
    },

    /* the checks for existence here are for use with game */

    uiToggler: function (t, k) {
      if (document.getElementById("scalesbuttons")) {
        document.getElementById("scalesbuttons").style.display = "none";
      }
      if (document.getElementById("intervalsbuttons")) {
        document.getElementById("intervalsbuttons").style.display = "none";
      }
      if (document.getElementById("keyboard_div")) {
        document.getElementById("keyboard_div").style.display = "none";
      }
      if (document.getElementById("melodiesbuttons")) {
        document.getElementById("melodiesbuttons").style.display = "none";
      }
      if (document.getElementById("main_controls_wrapper")) {
        document.getElementById("main_controls_wrapper").style.minHeight =
          "200px";
      }
      switch (t) {
        case "scales":
          document.getElementById("scalesbuttons").style.display = "block";
          break;
        case "intervals":
          document.getElementById("intervalsbuttons").style.display = "block";
          break;
        case "melodies":
          document.getElementById("melodiesbuttons").style.display = "block";
          break;
        case "keyboards":
          document.getElementById("keyboard_div").style.display = "block";
          // document.getElementById('clearStaff').style.display = "block";
          AVR.instrument_toggle(k);
          break;
      }
    },

    // for JS drop-down menus
    timeout: 250,
    closetimer: 0,
    ddmenuitem: 0,

    // cancel close timer

    mcancelclosetime: function () {
      if (AVR.closetimer) {
        window.clearTimeout(AVR.closetimer);
        AVR.closetimer = null;
      }
    },

    // open hidden layer

    mopen: function (id) {
      // cancel close timer
      AVR.mcancelclosetime();

      // close old layer
      if (AVR.ddmenuitem) {
        AVR.ddmenuitem.style.visibility = "hidden";
      }

      // get new layer and show it
      AVR.ddmenuitem = document.getElementById(id);
      AVR.ddmenuitem.style.visibility = "visible";
    },
    // close showed layer

    mclose: function () {
      if (AVR.ddmenuitem) {
        AVR.ddmenuitem.style.visibility = "hidden";
      }
    },

    // go close timer

    mclosetime: function () {
      AVR.closetimer = window.setTimeout(AVR.mclose, AVR.timeout);
    },

    // close layer when click-out
    // document.onclick = mclose;

    drawNote: function (note, ntrvl, mrgn, staff) {
      // which staff? (for game)
      if (staff === 2) {
        staff = document.getElementById("note_series2");
      } else {
        staff = document.getElementById("note_series1");
      }

      // x axis value for background position that determines the note glyph
      var noteGlyph,
        noteLi = document.createElement("li"),
        ledSpanA = document.createElement("span"),
        noteSpan = document.createElement("span"),
        ledSpanB = document.createElement("span");

      // 1. Draw first note
      noteLi.style.padding = 0;

      if (ntrvl === 0 && note > AVR.stemFlip) {
        // single note, top half of staff, stems down.
        noteGlyph = AVR.currentNotePattern[note] - 50;
      } else if (ntrvl > 7) {
        // large interval, long stem up
        noteGlyph = AVR.currentNotePattern[note] - 100;
      } else {
        // single note bottom of staff, or small interval, basic stems up
        noteGlyph = AVR.currentNotePattern[note];
      }

      if (AVR.noteMargins.length > 0) {
        noteLi.style.marginRight = mrgn + "px";
      }

      noteLi.style.backgroundPosition =
        noteGlyph + "px " + AVR.currentNotePositions[note] + "px";
      staff.appendChild(noteLi);

      // 2. Draw ledger lines
      ledSpanA.style.backgroundPosition =
        AVR.currentLedgerPattern[note] +
        "px " +
        AVR.currentLedgerPositions[note] +
        "px";
      if (ntrvl === 0) {
        // don't show captions for game
        if (AVR.currentNoteCaptions) {
          ledSpanA.innerHTML = AVR.currentNoteCaptions[note];
        }
      } else {
        ledSpanA.style.padding = 0;
      }
      staff.lastChild.appendChild(ledSpanA);

      // 2nd note needed?  (Interval?)
      if (!(ntrvl > 0)) {
        return;
      }

      // 3. Draw 2nd note
      noteSpan.style.padding = 0;

      // opposite stem side for small intervals
      if (ntrvl < AVR.currentBothStemSidesInterval) {
        noteGlyph = AVR.currentNotePattern[note + ntrvl] - 150;
        noteSpan.style.marginLeft = "15px";
      } else {
        noteGlyph = AVR.currentNotePattern[note + ntrvl];
      }
      noteSpan.style.backgroundPosition =
        noteGlyph + "px " + AVR.currentNotePositions[note + ntrvl] + "px";
      staff.lastChild.lastChild.appendChild(noteSpan);

      // 4. Draw 2nd note ledger lines
      ledSpanB.style.backgroundPosition =
        AVR.currentLedgerPattern[note + ntrvl] +
        "px " +
        AVR.currentLedgerPositions[note + ntrvl] +
        "px";
      // don't show captions for game
      if (AVR.intervalCaptions) {
        ledSpanB.innerHTML = AVR.intervalCaptions[ntrvl];
      }
      staff.lastChild.lastChild.lastChild.appendChild(ledSpanB);
    },

    // Note series functions
    drawNoteSeries: function () {
      var i;
      AVR.needsReset = true;
      for (i = 0; i < AVR.noteBank.length; i += 1) {
        AVR.drawNote(
          AVR.noteBank[i],
          AVR.currentInterval[i],
          AVR.noteMargins[i]
        );
      }
      document.getElementById("note_series1").scrollLeft = 0;
      // use this to get the HTML for initial page load.
      // alert(document.getElementById('note_series1').innerHTML);
    },

    halfSeconds: [
      500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500,
    ],
    fullSeconds: [
      1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000,
      1000, 1000,
    ],

    loadNoteSeries: function (
      whatNoteSeries,
      whatOffset,
      whatCaptions,
      whatTitle,
      whatInterval
    ) {
      var i;

      // stop any playback, and reset the play button
      AVR.togglePlayButton(0);
      AVR.clearStaff();

      // Defaults, may be changed below
      AVR.currentInterval = [0];
      AVR.noteMargins.length = 0;
      AVR.whatNoteMargin = null;
      document.getElementById("notes_title").innerHTML = whatTitle;

      switch (whatCaptions) {
        case 0:
          // captions for flats
          AVR.currentNoteCaptions = [
            "F<br>&nbsp;",
            "Gb<br>&nbsp;",
            "G<br>&nbsp;",
            "Ab<br>&nbsp;",
            "A<br>&nbsp;",
            "Bb<br>&nbsp;",
            "B<br>&nbsp;",
            "C<br>&nbsp;",
            "Db<br>&nbsp;",
            "D<br>&nbsp;",
            "Eb<br>&nbsp;",
            "E<br>&nbsp;",
            "F<br>&nbsp;",
            "Gb<br>&nbsp;",
            "G<br>&nbsp;",
            "Ab<br>&nbsp;",
            "A<br>&nbsp;",
            "Bb<br>&nbsp;",
            "B<br>&nbsp;",
            "C<br>&nbsp;",
            "Db<br>&nbsp;",
            "D<br>&nbsp;",
            "Eb<br>&nbsp;",
            "E<br>&nbsp;",
            "F<br>&nbsp;",
            "Gb<br>&nbsp;",
            "G<br>&nbsp;",
            "Ab<br>&nbsp;",
            "A<br>&nbsp;",
            "Bb<br>&nbsp;",
            "B<br>&nbsp;",
            "C<br>&nbsp;",
            "Db<br>&nbsp;",
            "D<br>&nbsp;",
            "Eb<br>&nbsp;",
            "E<br>&nbsp;",
            "F<br>&nbsp;",
          ];
          break;
        case 1:
          // captions for sharps
          AVR.currentNoteCaptions = [
            "F<br>&nbsp;",
            "F#<br>&nbsp;",
            "G<br>&nbsp;",
            "G#<br>&nbsp;",
            "A<br>&nbsp;",
            "A#<br>&nbsp;",
            "B<br>&nbsp;",
            "C<br>&nbsp;",
            "C#<br>&nbsp;",
            "D<br>&nbsp;",
            "D#<br>&nbsp;",
            "E<br>&nbsp;",
            "F<br>&nbsp;",
            "F#<br>&nbsp;",
            "G<br>&nbsp;",
            "G#<br>&nbsp;",
            "A<br>&nbsp;",
            "A#<br>&nbsp;",
            "B<br>&nbsp;",
            "C<br>&nbsp;",
            "C#<br>&nbsp;",
            "D<br>&nbsp;",
            "D#<br>&nbsp;",
            "E<br>&nbsp;",
            "F<br>&nbsp;",
            "F#<br>&nbsp;",
            "G<br>&nbsp;",
            "G#<br>&nbsp;",
            "A<br>&nbsp;",
            "A#<br>&nbsp;",
            "B<br>&nbsp;",
            "C<br>&nbsp;",
            "C#<br>&nbsp;",
            "D<br>&nbsp;",
            "D#<br>&nbsp;",
            "E<br>&nbsp;",
            "F<br>&nbsp;",
          ];
          break;
        case 2:
          // captions with both sharps and flats
          AVR.currentNoteCaptions = AVR.noteCaptions.slice();
          break;
      }

      switch (whatNoteSeries) {
        case 0:
          // chromatic scale
          AVR.noteBank = [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18];
          AVR.delayBank = AVR.halfSeconds.slice();
          AVR.whatNoteMargin = 22;
          break;

        case 1:
          // whole tone scales
          AVR.noteBank = [7, 9, 11, 13, 15, 17, 8, 10, 12, 14, 16, 18];
          AVR.delayBank = AVR.halfSeconds.slice();
          AVR.whatNoteMargin = 22;
          break;

        case 2:
          // C major scale
          AVR.noteBank = [7, 9, 11, 12, 14, 16, 18, 19];
          AVR.delayBank = AVR.halfSeconds.slice();
          AVR.whatNoteMargin = 35;
          break;

        case 3:
          // C minor scale
          AVR.noteBank = [7, 9, 10, 12, 14, 15, 17, 19];
          AVR.delayBank = AVR.halfSeconds.slice();
          AVR.whatNoteMargin = 35;
          break;

        case 4:
          // C blues scale
          AVR.noteBank = [7, 10, 12, 13, 14, 17, 19];
          // alt blues scale: [0,2,3,5,6,9,10,12];
          AVR.delayBank = AVR.halfSeconds.slice();
          AVR.whatNoteMargin = 35;
          break;

        case 5:
          // somewhere over the rainbow, in B
          AVR.noteBank = [
            18, 30, 29, 25, 27, 29, 30, 18, 27, 25, 15, 23, 22, 18, 20, 22, 23,
            20, 17, 18, 20, 22, 18,
          ];
          AVR.delayBank = [
            0, 1009, 1096, 543, 400, 352, 640, 721, 1119, 1048, 1784, 1225,
            1080, 552, 391, 400, 697, 927, 785, 447, 377, 712, 760,
          ];
          break;

        case 6:
          // do re mi, in C
          AVR.noteBank = [
            7, 9, 11, 7, 11, 7, 11, 9, 11, 12, 12, 11, 9, 12, 11, 12, 14, 11,
            14, 11, 14, 12, 14, 16, 16, 14, 12, 16, 14, 7, 9, 11, 12, 14, 16,
            16, 9, 11, 13, 14, 16, 18, 18, 11, 13, 15, 16, 18, 19, 18, 17, 16,
            12, 18, 14, 19, 14, 11, 9, 7, 7, 9, 11, 12, 14, 16, 18, 19, 7,
          ];
          AVR.delayBank = [
            0, 750, 250, 750, 250, 500, 500, 1000, 750, 250, 250, 250, 250, 250,
            2000, 750, 250, 750, 250, 500, 500, 1000, 750, 250, 250, 250, 250,
            250, 2000, 750, 250, 250, 250, 250, 250, 2000, 750, 250, 250, 250,
            250, 250, 2000, 750, 250, 250, 250, 250, 250, 1500, 250, 250, 500,
            500, 500, 500, 500, 500, 500, 500, 1000, 250, 250, 250, 250, 250,
            250, 250, 1000, 1000,
          ];
          break;

        case 7:
          // swingin on a gate, in G
          AVR.noteBank = [
            23, 25, 26, 23, 21, 18, 14, 16, 18, 19, 16, 18, 14, 16, 14, 11, 14,
            9, 14, 18, 21, 26, 26, 25, 26, 28, 26, 25, 21, 23, 25, 26, 23, 21,
            18, 14, 16, 18, 19, 16, 18, 14, 16, 14, 11, 14, 19, 16, 18, 14, 16,
            14, 11, 14, 9, 14, 14, 13, 14, 23, 25, 26, 25, 26, 28, 30, 28, 26,
            25, 26, 25, 23, 21, 23, 21, 19, 18, 11, 16, 18, 19, 18, 16, 14, 11,
            14, 16, 18, 19, 21, 23, 25, 26, 25, 26, 28, 30, 28, 26, 25, 26, 25,
            23, 21, 23, 21, 19, 18, 19, 16, 18, 14, 16, 14, 11, 14, 9, 14, 14,
            13, 14,
          ];
          AVR.delayBank = [
            0, 300, 300, 300, 300, 300, 300, 600, 300, 300, 300, 300, 300, 300,
            300, 300, 300, 300, 300, 300, 300, 300, 600, 600, 300, 300, 300,
            300, 300, 300, 300, 300, 300, 300, 300, 300, 600, 300, 300, 300,
            300, 300, 300, 300, 300, 300, 300, 300, 300, 300, 300, 300, 300,
            300, 300, 300, 300, 300, 300, 600, 300, 300, 300, 300, 300, 300,
            300, 300, 300, 300, 300, 300, 300, 300, 300, 300, 300, 300, 600,
            300, 300, 300, 300, 300, 300, 300, 300, 300, 300, 300, 300, 300,
            300, 300, 300, 300, 300, 300, 300, 300, 300, 300, 300, 300, 300,
            300, 300, 300, 300, 300, 300, 300, 300, 300, 300, 300, 300, 300,
            300, 300, 300, 600,
          ];
          break;

        case 8:
          // the entertainer
          AVR.noteBank = [
            33, 35, 31, 28, 30, 26, 21, 23, 19, 16, 18, 14, 9, 11, 7, 4, 6, 4,
            3, 2, 26, 9, 10, 11, 19, 11, 19, 11, 19, 31, 33, 34, 35, 31, 33, 35,
            30, 33, 31, 9, 10, 11, 19, 11, 19, 11, 19, 28, 26, 25, 28, 31, 35,
            33, 31, 28, 33, 9, 10, 11, 19, 11, 19, 11, 19, 31, 33, 34, 35, 31,
            33, 35, 30, 33, 31,
          ];
          AVR.delayBank = [
            0, 350, 350, 350, 700, 350, 700, 350, 350, 350, 700, 350, 700, 350,
            350, 350, 700, 350, 350, 350, 1300, 1500, 350, 350, 400, 600, 400,
            600, 400, 750, 350, 350, 350, 350, 350, 350, 700, 350, 600, 800,
            350, 350, 400, 600, 400, 600, 400, 750, 350, 350, 350, 350, 350,
            700, 350, 350, 400, 1000, 350, 350, 400, 600, 400, 600, 400, 750,
            350, 350, 350, 350, 350, 350, 700, 350, 800, 800,
          ];
          break;

        case 9:
          // fur elise
          AVR.noteBank = [
            23, 22, 23, 22, 23, 18, 21, 19, 16, 7, 11, 16, 18, 11, 15, 18, 19,
            11, 23, 22, 23, 22, 23, 18, 21, 19, 16, 7, 11, 16, 18, 11, 19, 18,
            16,
          ];
          AVR.delayBank = [
            0, 500, 500, 500, 500, 500, 500, 500, 500, 1500, 500, 500, 500,
            1500, 500, 500, 500, 1500, 500, 500, 500, 500, 500, 500, 500, 500,
            500, 1500, 500, 500, 500, 1500, 500, 500, 500, 1500, 500, 500, 500,
            500,
          ];
          break;

        case 10:
          // amazing grace, in A
          AVR.noteBank = [
            4, 9, 9, 13, 11, 9, 13, 11, 9, 6, 4, 4, 9, 9, 13, 11, 9, 13, 11, 16,
            16, 13, 16, 16, 13, 11, 9, 13, 11, 9, 6, 4, 4, 9, 9, 13, 11, 9, 13,
            11, 9,
          ];
          AVR.delayBank = [
            0, 450, 350, 1450, 250, 250, 300, 1450, 550, 1450, 550, 1450, 450,
            350, 1450, 250, 250, 300, 1450, 450, 450, 1350, 450, 400, 1400, 250,
            250, 300, 1450, 550, 1450, 550, 1450, 450, 350, 1450, 250, 250, 300,
            1450, 600, 1400,
          ];
          break;
      }

      // for changing the key of the scale
      if (whatOffset > 0) {
        for (i = 0; i < AVR.noteBank.length; i += 1) {
          AVR.noteBank[i] = AVR.noteBank[i] + whatOffset;
        }
      }

      // set delay for all intervals
      if (whatInterval > 0) {
        AVR.delayBank = AVR.fullSeconds.slice();

        // usual margins for intervals less than major 3rd
        // if (whatInterval < 4) {
        //    whatNoteMargin = null;
        // }
      }

      // for no interval (0) or whole tone scale intervals
      if (whatInterval < 13) {
        for (i = 0; i < AVR.noteBank.length; i += 1) {
          AVR.currentInterval[i] = whatInterval;
        }
      } else {
        // diatonic intervals
        switch (whatInterval) {
          case 20:
            AVR.currentInterval = [2, 2, 1, 2, 2, 2, 1, 2, 2, 1, 2, 2, 2, 1];
            break;
          case 30:
            AVR.currentInterval = [4, 3, 3, 4, 4, 3, 3, 4, 3, 3, 4, 4, 3, 3];
            break;
          case 40:
            AVR.currentInterval = [5, 5, 5, 6, 5, 5, 5, 5, 5, 5, 6, 5, 5, 5];
            break;
          case 50:
            AVR.currentInterval = [7, 7, 7, 7, 7, 7, 6, 7, 7, 7, 7, 7, 7, 6];
            break;
          case 60:
            AVR.currentInterval = [9, 9, 8, 9, 9, 8, 8, 9, 9, 8, 9, 9, 8, 8];
            break;
          case 70:
            AVR.currentInterval = [
              11, 10, 10, 11, 10, 10, 10, 11, 10, 10, 11, 10, 10, 10,
            ];
            break;
          case 80:
            AVR.currentInterval = [
              12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12,
            ];
            break;
        }

        // minor scale, so shift diatonic pattern 5 positions left
        if (whatNoteSeries === 3) {
          for (i = 0; i < AVR.noteBank.length; i += 1) {
            AVR.currentInterval[i] = AVR.currentInterval[i + 5];
          }
        }

        /*        
                // setting margins: major 3rds
                if (whatInterval === 30 && whatNoteSeries === 2) { 
                    noteMargins = Array (18, 44, 48, 26, 22, 44, 40, 22); 
                    whatNoteMargin = null;
                }
                // minor 3rds
                else if (whatInterval === 30 && whatNoteSeries === 3) { 
                    noteMargins = Array (40, 48, 20, 40, 46, 22,  18, 22); 
                    whatNoteMargin = null;
                }
                */
      }
      // set margins
      if (AVR.whatNoteMargin !== null) {
        for (i = 0; i < AVR.noteBank.length; i += 1) {
          AVR.noteMargins[i] = AVR.whatNoteMargin;
        }
      }

      // if Safari tiny delay to fix visual glitches
      if (AVR.sfri === false) {
        AVR.drawNoteSeries();
      } else {
        setTimeout(AVR.drawNoteSeries, 1);
      }
    },

    // when a note is played, play it, add it to noteBank and sends it to draw

    notePlayed: function (n) {
      AVR.playNote(n);
      // get current time as number
      AVR.thisTimestamp = Number(new Date());
      // clear staff if playback was done since
      // the last note was entered,
      // otherwise LONG duration between notes
      if (AVR.needsReset === true) {
        AVR.clearStaff();
        // document.getElementById('notes_title').innerHTML = "Notes Played on Keyboard";
        AVR.currentNoteCaptions = AVR.noteCaptions.slice();
        AVR.drawNote(n, 0, 22);
        AVR.noteBank.length = 0;
        AVR.delayBank.length = 0;
        AVR.noteCount = 0;
        AVR.currentInterval.length = 0;
        AVR.currentInterval[0] = 0;
        AVR.noteMargins.length = 0;
        AVR.needsReset = false;
        // stop the playback if it is on, and reset the play button
        AVR.togglePlayButton(0);
      } else {
        AVR.drawNote(n, 0, 22);
      }
      document.getElementById("note_series1").scrollLeft = 1000000;
      AVR.noteBank[AVR.noteCount] = n;
      AVR.delayBank[AVR.noteCount] = AVR.thisTimestamp - AVR.lastTimestamp;
      AVR.currentInterval[AVR.noteCount] = 0;
      AVR.noteCount += 1;
      AVR.lastTimestamp = AVR.thisTimestamp;
    },

    // using if statement & recursion to play a series of notes or intervals
    // as long as there are more notes to play in noteBank
    // the function keeps calling itself at a given delay to correctly time the notes
    // if another event sets playBackOn === false, that will end the recursion loop
    // pbc is "play back count"
    //

    playLoop: function () {
      if (AVR.playBackOn === true) {
        if (AVR.currentInterval[AVR.pbc] === 0) {
          AVR.playNote(AVR.noteBank[AVR.pbc]);
        } else {
          AVR.playNote(AVR.noteBank[AVR.pbc]);
          AVR.playNote(AVR.noteBank[AVR.pbc] + AVR.currentInterval[AVR.pbc]);
        }

        AVR.drawNote(
          AVR.noteBank[AVR.pbc],
          AVR.currentInterval[AVR.pbc],
          AVR.noteMargins[AVR.pbc]
        );
        document.getElementById("note_series1").scrollLeft = 1000000;
        AVR.pbc += 1;

        if (AVR.pbc < AVR.noteBank.length) {
          AVR.pbtimeout = setTimeout(AVR.playLoop, AVR.delayBank[AVR.pbc]);
        } else {
          AVR.pbc = 0;
          AVR.togglePlayButton(0);
        }
      }
    },

    playback: function () {
      // for constructing melodies:
      // alert(noteBank);

      // notes to play?
      if (AVR.noteBank.length > 0) {
        // allows playback, and toggles the play button to "stop playback"
        AVR.togglePlayButton(1);
        document.getElementById("note_series1").style.overflow = "hidden";
        AVR.needsReset = true;
        AVR.clearStaff();
        // avoids colliding note traffic jam, try-catch is for IE8
        // try{silence.play()}
        // catch(e){};
        AVR.pbc = 0;
        setTimeout(AVR.playLoop, 500);
      } else {
        alert("There are no notes to play.  Enter some notes and try again.");
      }
    },

    // IE8 chokes on the audio tag functions, and doesn't process the rest of the script,
    // so moved to end so that it will execute onload functions (no longer the case with howler.js?)

    // Notation controls
    currentNoteShape: "twinnote",
    twinAdjust: 63,
    lineName: [
      "F",
      "F# / Gb",
      "G",
      "G# / Ab",
      "A",
      "A# / Bb",
      "B",
      "C",
      "C# / Db",
      "D",
      "D# / Eb",
      "E",
      "F",
    ],
    inverter: [0, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
    currentStaffLevel: 11,

    setNotation: function (staffPattern, notePattern) {
      if (staffPattern !== "x") {
        document.getElementById("notecontainer1").style.backgroundImage =
          "url(https://twinnote.clairnote.org/wp/wp-content/themes/twinnote/images/TWNTaudiovisualizer/staff-twin-" +
          staffPattern +
          ".png)";

        // change second staff for game
        if (document.getElementById("notecontainer2")) {
          document.getElementById("notecontainer2").style.backgroundImage =
            "url(https://twinnote.clairnote.org/wp/wp-content/themes/twinnote/images/TWNTaudiovisualizer/staff-twin-" +
            staffPattern +
            ".png)";
        }
      }
      switch (
        notePattern // set note pattern
      ) {
        case "x":
          break;
        case 0:
          AVR.currentNotePattern = AVR.twinNoteNotes.slice();
          break;
        case 1:
          AVR.currentNotePattern = AVR.allSolidNotes.slice();
          break;
        case 2:
          AVR.currentNotePattern = AVR.allHollowNotes.slice();
          break;
      }
      // if not game page, reset staff
      if (!document.getElementById("notecontainer2")) {
        AVR.togglePlayButton(0); // stop any playback, and reset the play button
        AVR.clearStaff();
        AVR.drawNoteSeries();
      }
    },
  };
})();
AVR.audioCheck();

// show controls
document.getElementById("load-content").style.display = "block";

if (AVR.piano === null) {
  AVR.loadUI();
}

// IE8 chokes on this, so putting it here.
try {
  if (navigator.vendor.indexOf("Apple") !== -1) {
    AVR.sfri = true;
  } // detect Safari browser, ugh.
} catch (ignore) {}
