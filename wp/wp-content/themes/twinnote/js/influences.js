/* (c) 2013 Paul Morris */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */

//javascript document

// Influences
imagePaths = [
"http://twinnote.org/wp-content/uploads/2011/07/Ailler-chromatic.png", 
"http://twinnote.org/wp-content/uploads/2011/07/Beyreuther-chromatic.png", 
"http://twinnote.org/wp-content/uploads/2011/07/Equiton-chromatic.png", 
"http://twinnote.org/wp-content/uploads/2011/07/Twinline-chromatic.png", 
"http://twinnote.org/wp-content/uploads/2011/07/TwinlineBlackOval-chromatic.png", 
"http://twinnote.org/wp-content/uploads/2011/07/TwinNote-chromatic.png", 

"http://twinnote.org/wp-content/uploads/2011/07/Ailler-Cmajor.png", 
"http://twinnote.org/wp-content/uploads/2011/07/Beyreuther-Cmajor.png", 
"http://twinnote.org/wp-content/uploads/2011/07/Equiton-Cmajor.png", 
"http://twinnote.org/wp-content/uploads/2011/07/Twinline-Cmajor.png", 
"http://twinnote.org/wp-content/uploads/2011/07/TwinlineBlackOval-Cmajor.png", 
"http://twinnote.org/wp-content/uploads/2011/07/TwinNote-Cmajor.png", 

"http://twinnote.org/wp-content/uploads/2011/07/Ailler-6-6.png", 
"http://twinnote.org/wp-content/uploads/2011/07/Beyreuther-6-6.png", 
"http://twinnote.org/wp-content/uploads/2011/07/Equiton-6-6.png", 
"http://twinnote.org/wp-content/uploads/2011/07/Twinline-6-3-3.png", 
"http://twinnote.org/wp-content/uploads/2011/07/TwinlineBlackOval-6-3-3.png", 
"http://twinnote.org/wp-content/uploads/2011/07/TwinNote-6-6.png", 

"http://twinnote.org/wp-content/uploads/2011/07/Ailler-5ths-6ths.png", 
"http://twinnote.org/wp-content/uploads/2011/07/Beyreuther-5ths-6ths.png", 
"http://twinnote.org/wp-content/uploads/2011/07/Equiton-5ths-6ths.png", 
"http://twinnote.org/wp-content/uploads/2011/07/Twinline-5ths-6ths.png", 
"http://twinnote.org/wp-content/uploads/2011/07/TwinlineBlackOval-5ths-6ths.png", 
"http://twinnote.org/wp-content/uploads/2011/07/TwinNote-5ths-6ths.png" 
]


altText = [
"Chromatic Scale in Ailler's Untitled music notation", 
"Chromatic Scale in Beyreuther's Untitled music notation", 
"Chromatic Scale in Fawcett's Equiton music notation", 
"Chromatic Scale in Twinline music notation", 
"Chromatic Scale in Black-Oval Twinline music notation", 
"Chromatic Scale in TwinNote music notation", 

"C Major Scale in Ailler's Untitled music notation", 
"C Major Scale in Beyreuther's Untitled music notation", 
"C Major Scale in Fawcett's Equiton music notation", 
"C Major Scale in Twinline music notation", 
"C Major Scale in Black-Oval Twinline music notation", 
"C Major Scale in TwinNote music notation", 

"6-6 Pitch Pattern in Ailler's Untitled music notation", 
"6-6 Pitch Pattern in Beyreuther's Untitled music notation", 
"6-6 Pitch Pattern in Fawcett's Equiton music notation", 
"6-6 Pitch Pattern in Twinline music notation", 
"6-6 Pitch Pattern in Black-Oval Twinline music notation", 
"6-6 Pitch Pattern in TwinNote music notation", 

"5ths and Minor 6ths in Ailler's Untitled music notation", 
"5ths and Minor 6ths in Beyreuther's Untitled music notation", 
"5ths and Minor 6ths in Fawcett's Equiton music notation", 
"5ths and Minor 6ths in Twinline music notation", 
"5ths and Minor 6ths in Black-Oval Twinline music notation", 
"5ths and Minor 6ths in TwinNote music notation"
]

var swapper = function(selection, where, width, height) {
	// console.log(selection);
	document.getElementById(where).innerHTML = 
		"<img name=\"scales\" src=\"" + imagePaths[selection] 
		+ "\" width=\"" + width 
		+ "\" height=\"" + height 
		+ "\" border=\"0\" id=\"scales_r2_c\" alt=\"" + altText[selection] 
		+ "\" />";     
}

// Preloading images 
var preloader = function() {
	 var i = 0,
         imageObj = [];

	for(i=0; i <= imagePaths.length-1; i += 1) {
		imageObj[i] = new Image();
		imageObj[i].src=imagePaths[i];
	}

} 

window.onload = preloader();