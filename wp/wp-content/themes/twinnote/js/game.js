/* (c) 2013 Paul Morris */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/. */
// JavaScript Document

// fix for IE8 not supporting indexOf for arrays.
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
        "use strict";
        if (this == null) {
            throw new TypeError();
        }
        var t = Object(this);
        var len = t.length >>> 0;
        if (len === 0) {
            return -1;
        }
        var n = 0;
        if (arguments.length > 1) {
            n = Number(arguments[1]);
            if (n != n) { // shortcut for verifying if it's NaN
                n = 0;
            } else if (n != 0 && n != Infinity && n != -Infinity) {
                n = (n > 0 || -1) * Math.floor(Math.abs(n));
            }
        }
        if (n >= len) {
            return -1;
        }
        var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
        for (; k < len; k++) {
            if (k in t && t[k] === searchElement) {
                return k;
            }
        }
        return -1;
    }
}

// TNG = TwinNoteGame
var TNG = (function () {
    "use strict";
    return {

        noteSet: [
            [7, 9, 11, 12, 14, 16, 18], // [0] 1 octave: 7 natural notes 
            [8, 10, 13, 15, 17], // [1] 1 octave: 5 sharps and flats 
            [0, 2, 4, 5, 7, 9, 11], // [2] for 1 octave: all 
            [7, 9, 11, 12, 14, 16, 18, 19, 21, 23, 24, 26, 28, 30], // [3] 2 octaves: 14 natural notes
            [8, 10, 13, 15, 17, 20, 22, 25, 27, 29], // [4] 2 octaves: 10 sharps and flats
            [0, 2, 4, 5, 7, 9, 11, 12, 14, 16, 17, 19, 21, 23], // [5] for 2 octaves: all
            [0, 2, 4, 5, 7, 9, 11] // [6] intervals
            // [0,1,2,3,4,5,6,
            // 7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,
            // 31,32,33,34,35,36] // [7] all 37 possible notes
        ],
        currentNoteSet: 0,
        noteBank: [],
        targetNotes: [],

        // select a key at random for all notes game
        key: Math.floor(Math.random() * 12),
        keyChange: [4, 6, 9, 11],

        noteCaptions: ["F", "F# / Gb", "G", "G# / Ab", "A", "A# / Bb", "B", "C", "C# / Db", "D", "D# / Eb", "E", "F", "F# / Gb", "G", "G# / Ab", "A", "A# / Bb", "B", "C", "C# / Db", "D", "D# / Eb", "E", "F", "F# / Gb", "G", "G# / Ab", "A", "A# / Bb", "B", "C", "C# / Db", "D", "D# / Eb", "E", "F"],

        intervalMode: false,
        intervalSet: [
            [2, 4, 6, 8, 10, 12], // [0] even intervals
            [1, 3, 5, 7, 9, 11], // [1] odd intervals
            [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12] // [2] all intervals
        ],

        currentIntervalSet: 0,
        intervalBank: [],
        noteBankB: [],
        targetIntervals: [],

        intervalCaptions: [0, 'Minor 2nd', 'Major 2nd', 'Minor 3rd', 'Major 3rd', 'Perfect 4th', 'Tritone', 'Perfect 5th', 'Minor 6th', 'Major 6th', 'Minor 7th', 'Major 7th', 'Octave'],

        // number of target notes or intervals at a time on the staff
        totalTargetsOnStaff: 4,
        correctNotesOnStaff: 0,

        feedback: document.getElementById('feedback'),
        feedbackTimer: undefined,
        gameSelector: document.getElementById("gameSelector"),

        rightTally: 0,
        wrongTally: 0,
        totalTally: 0,
        totalTallyElem: document.getElementById('totalTally'),

        countdown: 0,
        countdownElem: document.getElementById("countdown"),

        timeoutID: undefined,
        noteSeries2Elem: document.getElementById("note_series2"),
        firstTry: true,

        // pick an item/value at random from sourceArray and if it is not the 
        // same number as the last 3 numbers in targetArray, return that number
        // if it is the same, else recursively call the function again...
        //
        getNewRandom: function (sourceArray, targetArray) {
            var r = Math.floor(Math.random() * sourceArray.length),
                n = sourceArray[r];
            if (n !== targetArray[targetArray.length - 1] &&
                    n !== targetArray[targetArray.length - 2] &&
                    n !== targetArray[targetArray.length - 3]) {
                return r;
            }
            return TNG.getNewRandom(sourceArray, targetArray);
        },

        /* 
            in reloadNoteBank the currentNoteSet is added twice to tempBank, 
            if we're playing an "all notes" game the notes are transposed to a
            semi-random key.  keyChange limits this to key changes that contain
            3 or more new notes that weren't in the last key for good note coverage.
            Then the order of the notes is randomized using a recursive function 
            that moves notes from tempBank to TNG.noteBank.  We have to stop when there 
            are only 3 notes left in tempBank to avoid possibility of infinite recursion
            where the only notes left to choose from are the same as the last 3 notes chosen.  
        */
        reloadNoteBank: function () {
            var tempBank = TNG.noteSet[TNG.currentNoteSet].slice(),
                m,
                j,
                len,
                i,
                index;
            // double up for non-interval games
            if (TNG.currentNoteSet !== 6) {
                tempBank = tempBank.concat(TNG.noteSet[TNG.currentNoteSet].slice());
            }

            // for "all notes" games and interval games
            if (TNG.currentNoteSet === 2 || TNG.currentNoteSet === 5 || TNG.currentNoteSet === 6) {
                TNG.key = (TNG.key + TNG.keyChange[Math.floor(Math.random() * 4)]) % 12;
                m = (TNG.currentNoteSet === 5) ? 24 : 12;
                for (j = 0; j < tempBank.length; j += 1) {
                    tempBank[j] = ((tempBank[j] + TNG.key) % m) + 7;
                }
            }

            if (TNG.currentNoteSet === 6) {
                TNG.noteBank = tempBank.slice();
            } else {
                // randomize the order of notes    
                // for 2-octave all notes, limit length so 
                // we aren't in the same key forever
                if (TNG.currentNoteSet === 5) {
                    len = tempBank.length - 3 - 14;
                } else {
                    len = tempBank.length - 3;
                }

                for (i = 0; i < len; i += 1) {
                    // pick a random note from tempBank 
                    // add it to noteBank
                    // remove it from tempBank
                    index = TNG.getNewRandom(tempBank, TNG.noteBank);
                    TNG.noteBank.push(tempBank[index]);
                    tempBank.splice(index, 1);
                }
            }
        },


        reloadIntervalBank: function () {
            var tempBank = TNG.intervalSet[TNG.currentIntervalSet].slice(),
                i,
                len,
                index;
            tempBank = tempBank.concat(TNG.intervalSet[TNG.currentIntervalSet].slice());

            len = tempBank.length - 3;
            for (i = 0; i < len; i += 1) {
                // pick a semi-random interval from intervalBank 
                // add it to targetIntervals
                // remove it from intervalBank
                index = TNG.getNewRandom(tempBank, TNG.intervalBank);
                TNG.intervalBank.push(tempBank[index]);
                tempBank.splice(index, 1);
            }
        },

        // interval is the interval
        // notesA is the array to search for the lower note
        // notesB is the array to search for the higher note
        findInterval: function (interval, notesA, notesB) {
            var r = Math.floor(Math.random() * notesA.length),
                lowNote = notesA[r];
            if (notesB.indexOf(lowNote + interval) !== -1) {
                return lowNote;
            }

            notesA.splice(r, 1);
            if (notesA.length === 0) {
                return false;
            }
            return TNG.findInterval(interval, notesA.slice(), notesB.slice());
        },

        // displays a series of target notes/intervals chosen semi-randomly
        // notesAtOnce is the number of notes to show on the staff
        showTargetNotes: function (notesAtOnce) {
            var i, j, lowNote;
            // is game on?
            if (TNG.countdown > 0) {
                for (i = 0; i < notesAtOnce; i += 1) {
                    if (TNG.intervalMode === false) {
                        // reload TNG.noteBank if it is getting low
                        if (TNG.noteBank.length < 4) {
                            TNG.reloadNoteBank();
                        }
                        TNG.targetNotes.push(TNG.noteBank[0]);
                        TNG.noteBank.splice(0, 1);
                        AVR.drawNote(TNG.targetNotes[TNG.targetNotes.length - 1], 0, null);
                    } else {
                        // console.log("NB: " + TNG.noteBank);
                        // reload intervalBank if needed
                        if (TNG.intervalBank.length < 4) {
                            TNG.reloadIntervalBank();
                        }

                        // get notes to go with the interval
                        // TNG.noteBank stays the same, so this is only
                        // called at the beginning of game.
                        if (TNG.noteBank.length < 4) {
                            TNG.reloadNoteBank();

                            // load TNG.noteBankB for 2nd/higher note of intervals
                            // it has a range of 2 octaves instead of TNG.noteBank's one
                            TNG.noteBankB = TNG.noteBank.slice();
                            for (j = 0; j < TNG.noteBankB.length; j += 1) {
                                TNG.noteBankB[j] = TNG.noteBankB[j] + 12;
                            }
                            TNG.noteBankB = TNG.noteBankB.concat(TNG.noteBank.slice());
                        }

                        lowNote = TNG.findInterval(TNG.intervalBank[0], TNG.noteBank.slice(), TNG.noteBankB.slice());

                        if (lowNote !== false) {
                            TNG.targetNotes.push(lowNote);
                            // if you want TNG.noteBank to change:
                            // TNG.noteBank.splice(TNG.noteBank.indexOf(m) , 1);
                            // but that means sometimes intervals won't be found, ugh.
                            TNG.targetIntervals.push(TNG.intervalBank[0]);
                            TNG.intervalBank.splice(0, 1);
                            AVR.drawNote(TNG.targetNotes[TNG.targetNotes.length - 1], TNG.targetIntervals[TNG.targetIntervals.length - 1], null);
                        } else {
                            // this should never happen since TNG.noteBank stays the same
                            TNG.intervalBank.splice(0, 1);
                            i -= 1;
                        }
                    }
                }
            }
        },

        keyPressed: function (n, i) {
            var elems;
            // check if game is on
            if (TNG.countdown > 0) {

                // if it's not the first attempt, clear the previous note/interval
                if (TNG.firstTry !== true) {
                    elems = TNG.noteSeries2Elem.getElementsByTagName('li');
                    elems[0].parentNode.removeChild(elems[elems.length - 1]);
                }

                if (TNG.intervalMode === true) {
                    AVR.playNote(TNG.targetNotes[0]);
                    AVR.playNote(TNG.targetNotes[0] + i);
                    AVR.drawNote(TNG.targetNotes[0], i, null, 2);
                    TNG.feedback.style.paddingLeft = ((TNG.correctNotesOnStaff * 67) + 20) + "px";
                } else {
                    AVR.playNote(n);
                    AVR.drawNote(n, 0, null, 2);
                    TNG.feedback.style.paddingLeft = ((TNG.correctNotesOnStaff * 67) + 33) + "px";
                }

                // correct note/interval?
                if ((TNG.intervalMode === false && n === TNG.targetNotes[0]) ||
                        (TNG.intervalMode === true && i === TNG.targetIntervals[0])) {

                    TNG.feedback.style.color = "green";
                    TNG.feedback.innerHTML = "Yes! " +
                        ((TNG.intervalMode === true) ? TNG.intervalCaptions[i] : TNG.noteCaptions[n]);

                    // remove the note from TNG.targetNotes/TNG.targetIntervals
                    TNG.targetNotes.shift();
                    if (TNG.intervalMode === true) {
                        TNG.targetIntervals.shift();
                    }

                    TNG.tallyUpdate(1, 0);
                    TNG.firstTry = true;
                    TNG.correctNotesOnStaff += 1;

                    clearTimeout(TNG.feedbackTimer);
                    TNG.feedbackTimer = setTimeout(TNG.clearFeedback, 1000);

                    // reload the staff?
                    if (TNG.targetNotes.length === 0) {
                        TNG.correctNotesOnStaff = 0;
                        setTimeout(AVR.clearStaff, 500);
                        setTimeout(function () {AVR.clearStaff(2); }, 500);
                        setTimeout(function () {TNG.showTargetNotes(TNG.totalTargetsOnStaff); }, 700);
                        setTimeout(function () {TNG.firstTry = true; }, 750);
                        setTimeout(TNG.clearFeedback, 750);
                    }
                } else {
                    clearTimeout(TNG.feedbackTimer);
                    TNG.feedback.style.color = "red";

                    if (TNG.intervalMode === true) {
                        TNG.feedback.innerHTML =
                            ((i < TNG.targetIntervals[0]) ? "Too small..." : "Too large...");
                    } else {
                        TNG.feedback.innerHTML =
                            ((n < TNG.targetNotes[0]) ? "Too low..." : "Too high...");
                    }

                    TNG.tallyUpdate(0, 1);
                    TNG.firstTry = false;
                }
            } else {
                if (TNG.intervalMode === false) {
                    // game is not on, so let them play the piano
                    AVR.playNote(n);
                }
            }
        },

        tallyUpdate: function (right, wrong) {
            if (right !== 0) {
                TNG.rightTally += right;
                // totalTallyElem.title = "correct: " + TNG.rightTally + " incorrect: " + TNG.wrongTally;
            } else if (wrong !== 0) {
                TNG.wrongTally += wrong;
            }
            document.getElementById("score").title = TNG.rightTally + " correct, " + TNG.wrongTally + " incorrect";
            TNG.totalTally = TNG.rightTally - TNG.wrongTally;
            TNG.totalTallyElem.innerHTML = TNG.totalTally;
        },

        clearFeedback: function () {
            if (TNG.countdown > 0) {
                TNG.feedback.innerHTML = "&nbsp;";
            }
        },

        // notes is integer 0-5 indicating what noteSet to use
        // intervals is integer 0-2 indicating what intervalSet to use
        changeGame: function (notes, intervals) {
            TNG.resetGame();
            TNG.currentNoteSet = notes;
            TNG.noteBank = [];

            if (intervals !== undefined) { // intervals
                TNG.currentIntervalSet = intervals;
                TNG.intervalBank = [];
                TNG.intervalMode = true;
                document.getElementById('intervalsbuttons').style.display = "block";
                document.getElementById('keyboard_div').style.display = "none";
                switch (TNG.currentIntervalSet) {
                case 0:
                    TNG.gameSelector.innerHTML = "Intervals: Even Number of Semitones";
                    document.getElementById('odd-intervals').style.display = "none";
                    document.getElementById('even-intervals').style.paddingLeft = "0px";
                    document.getElementById('even-intervals').style.display = "inline";
                    break;
                case 1:
                    TNG.gameSelector.innerHTML = "Intervals: Odd Number of Semitones";
                    document.getElementById('odd-intervals').style.display = "inline";
                    document.getElementById('even-intervals').style.display = "none";
                    break;
                case 2:
                    TNG.gameSelector.innerHTML = "Intervals: All";
                    document.getElementById('odd-intervals').style.display = "inline";
                    document.getElementById('even-intervals').style.paddingLeft = "45px";
                    document.getElementById('even-intervals').style.display = "inline";
                    break;
                }
            } else { // notes
                TNG.intervalMode = false;
                document.getElementById('intervalsbuttons').style.display = "none";
                document.getElementById('keyboard_div').style.display = "block";

                document.getElementById('piano-octave2a').style.display = "none";
                document.getElementById('piano-octave2b').style.display = "none";
                document.getElementById('string-octave2').style.display = "none";
                document.getElementById('sixsix-octave2a').style.display = "none";
                document.getElementById('sixsix-octave2b').style.display = "none";

                switch (TNG.currentNoteSet) {
                case 0:
                    TNG.gameSelector.innerHTML = "Notes: 1 Octave: Naturals";
                    break;
                case 1:
                    TNG.gameSelector.innerHTML = "Notes: 1 Octave: Sharps & Flats";
                    break;
                case 2:
                    TNG.gameSelector.innerHTML = "Notes: 1 Octave: All";
                    break;
                case 3:
                    TNG.gameSelector.innerHTML = "Notes: 2 Octaves: Naturals";
                    document.getElementById('piano-octave2a').style.display = "inline";
                    document.getElementById('piano-octave2b').style.display = "inline";
                    document.getElementById('string-octave2').style.display = "inline";
                    document.getElementById('sixsix-octave2a').style.display = "inline";
                    document.getElementById('sixsix-octave2b').style.display = "inline";
                    break;
                case 4:
                    TNG.gameSelector.innerHTML = "Notes: 2 Octaves: Sharps & Flats";
                    document.getElementById('piano-octave2a').style.display = "inline";
                    document.getElementById('piano-octave2b').style.display = "inline";
                    document.getElementById('string-octave2').style.display = "inline";
                    document.getElementById('sixsix-octave2a').style.display = "inline";
                    document.getElementById('sixsix-octave2b').style.display = "inline";
                    break;
                case 5:
                    TNG.gameSelector.innerHTML = "Notes: 2 Octaves: All";
                    document.getElementById('piano-octave2a').style.display = "inline";
                    document.getElementById('piano-octave2b').style.display = "inline";
                    document.getElementById('string-octave2').style.display = "inline";
                    document.getElementById('sixsix-octave2a').style.display = "inline";
                    document.getElementById('sixsix-octave2b').style.display = "inline";
                    break;
                case 7:
                    TNG.gameSelector.innerHTML = "Notes: 3 Octaves: All";
                    break;
                }
            }
            AVR.clearStaff();
            AVR.clearStaff(2);
            TNG.feedback.innerHTML = "&nbsp;";
        },

        changeTotalTargets: function (x) {
            document.getElementById('totalTargetsControl').innerHTML = x + " at a Time";
            TNG.totalTargetsOnStaff = x;
        },

        countItDown: function () {
            TNG.countdown -= 1;
            TNG.countdownElem.innerHTML = TNG.countdown;

            if (TNG.countdown > 0) {
                TNG.timeoutID = setTimeout(TNG.countItDown, 1000);
            } else { // end game    
                AVR.clearStaff();
                AVR.clearStaff(2);
                TNG.feedback.style.paddingLeft = "33px";
                TNG.feedback.style.color = "black";
                TNG.feedback.innerHTML = "Game Over &mdash; Score: " + TNG.totalTally;
                TNG.correctNotesOnStaff = 0;
                TNG.timeoutID = setTimeout(function () {TNG.feedback.innerHTML = "&nbsp;"; }, 3000);
                document.getElementById('startButton').innerHTML = "Start";
                TNG.firstTry = true;
                TNG.targetNotes = [];
                TNG.targetIntervals = [];
                TNG.noteBank = [];
                TNG.intervalBank = [];
            }
        },

        // start new game
        startButton: function () {
            AVR.clearStaff();
            AVR.clearStaff(2);
            TNG.feedback.innerHTML = "&nbsp;";

            if (TNG.countdown === 0) {
                // start
                TNG.countdown = 60;
                TNG.countItDown();
                TNG.resetScore();
                document.getElementById('startButton').innerHTML = "Stop";
                TNG.showTargetNotes(TNG.totalTargetsOnStaff);
            } else {
                // stop
                TNG.resetGame();
            }
        },

        resetGame: function () {
            clearTimeout(TNG.timeoutID);
            AVR.clearStaff();
            AVR.clearStaff(2);
            TNG.feedback.innerHTML = "&nbsp;";
            TNG.countdown = 0;
            TNG.countdownElem.innerHTML = 0;
            document.getElementById('startButton').innerHTML = "Start";
            TNG.firstTry = true;
            TNG.targetNotes = [];
            TNG.targetIntervals = [];
            TNG.noteBank = [];
            TNG.intervalBank = [];
            TNG.correctNotesOnStaff = 0;
        },

        resetScore: function () {
            TNG.rightTally = 0;
            TNG.wrongTally = 0;
            TNG.totalTally = 0;
            document.getElementById('score').title = "0 correct, 0 incorrect";
            document.getElementById('totalTally').innerHTML = 0;
        },

        // adjust note and ledger position values to match the game staves
        redoPositions: function (array) {
            var i;
            for (i = 0; i < array.length; i += 1) {
                array[i] = array[i] - 60;
            }
        }
    };
}());

///////////////////////////////////////////////////////
// redoing things from TWNTaudiovisualizer.js

// set to null so they are not shown by AVR.drawNote()
// AVR.drawNote() checks for null or not
AVR.intervalCaptions = null;
AVR.currentNoteCaptions = null;
AVR.noteMargins.length = 0;
TNG.redoPositions(AVR.currentNotePositions);
TNG.redoPositions(AVR.currentLedgerPositions);