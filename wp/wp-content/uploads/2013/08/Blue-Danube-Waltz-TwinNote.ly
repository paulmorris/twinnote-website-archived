\version "2.17.19"

\include "twinnote-scripts.ly"

\pointAndClickOff

% The Blue Danube Waltz
% Johann Strauss Jr.
% 
% typeset by Nikos Kouremenos <kourem AT gmail DOT com>
% source by Christian Mondrup Four Hand Piano reduction (Werner Icking Music Archive)
% also transposed to C major and some other changes. The main theme only (one page)
% so beginners will love me :)
% the 4hand full original can be downloaded from 
% http://icking-music-archive.org/ByComposer/J.Strauss.html
%
% The original file from the Mutopia Project was last updated on 2005-1-12.
%
% This music is part of the Mutopia project (http://www.MutopiaProject.org/).
% Copyright (c) The Mutopia Project and Nikos Kouremenos, 2005.
%
% This work is licensed under the Creative Commons Attribution-ShareAlike License 2.0.
% To view a copy of that license visit
% http://creativecommons.org/licenses/by-sa/2.0/ or send a letter to Creative Commons,
% 559 Nathan Abbott Way, Stanford, California 94305, USA.
% 

\header {
  title = "The Blue Danube Waltz"
  composer = "Johann Strauss Jr. (1825 - 1899)"

  mutopiatitle = "The Blue Danube Waltz"
  mutopiacomposer = "StraussJJ"
  mutopiaopus = "O 314"
  mutopiainstrument = "Piano"
  date = "1867"
  source = "Christian Mondrup"
  style = "Romantic"
  copyright = "Creative Commons Attribution-ShareAlike 2.0"
  maintainer = "Nikos Kouremenos"
  maintainerEmail = "kourem@gmail.com"
  maintainerWeb = "http://members.hellug.gr/nkour/"
  lastupdated = "2005/Jan/17"

  footer = "Mutopia-2005/01/17-519"
  tagline = \markup { \override #'(box-padding . 1.0) \override #'(baseline-skip . 2.7) \box \center-column { \small \line { Sheet music from \with-url #"http://www.MutopiaProject.org" \line { \concat { \teeny www. \normalsize MutopiaProject \teeny .org } \hspace #0.5 } • \hspace #0.5 \italic Free to download, with the \italic freedom to distribute, modify and perform. } \line { \small \line { Typeset using \with-url #"http://www.LilyPond.org" \line { \concat { \teeny www. \normalsize LilyPond \teeny .org }} by \concat { \maintainer . } \hspace #0.5 Reference: \footer } } \line { \small \line { Transcribed into \with-url #"http://twinnote.org" TwinNote music notation using \with-url #"http://www.LilyPond.org" \line { \concat { \teeny www. \normalsize LilyPond \teeny .org }} by Paul Morris. \hspace #0.5 Details: \with-url #"http://twinnote.org" http://twinnote.org }} \line { \teeny \line { Licensed under the Creative Commons Attribution-ShareAlike 2.0 (Unported) License, for details \concat { see: \hspace #0.3 \with-url #"http://creativecommons.org/licenses/by-sa/2.0" http://creativecommons.org/licenses/by-sa/2.0 } } } } }
}

%%
%% MUSIC
%%


middleDynamics = {
  % the first piano is set in soprano even though centered
  % other dynamics that are not centered will be found in their voices
  \time 3/4
  \partial 4

  % bar 1
  s1*0 \mf s4

  % bar 2 - 16
  s2.*15

  % bar 17
  s4 s4_\markup { "cresc." } s4 |

  % bar 18 - 22
  s2.*5

  % bar 23
  s4_\markup { "cresc." } s2 |

  % bar 24
  s2.

  % bar 25
  s2 s4.\f

}

upper =  {
  \relative c' {
    \clef treble
    \time 3/4
    \key c \major

    % bar 1
    \partial 4
    c4

    % bar 2
    c( e g)

    % bar 3
    g2 <e' g>4

    % bar 4
    <e g>2
    <c e>4

    % bar 5
    <c e>2
    c,4

    % bar 6
    c( e g)
    
    % bar 7
    g2 <f' g>4


    % bar 8
    <f g>2
    <b, f'>4

    % bar 9
    <b f'>2
    b,4

    % bar 10
    b( d a')

    % bar 11
    a2
    <f' a>4

    % bar 12
    <f a>2
    <b, f'>4
    
    % bar 13
    <b f'>2
    b,4

    % bar 14
    b( d a')


    % bar 15
    a2
    <e' a>4

    % bar 16
    <e a>2
    <c e>4

    % bar 17
    <c e>2
    c,4

    % bar 18
    c( e g)

    % bar 19
    c2
    <g' c>4

    % bar 20
    <g c>2
    <e g>4

    % bar 21
    <e g>2
    c,4

    % bar 22
    c( e g)

    % bar 23
    c2
    <a' c>4

    % bar 24
    <a c>2
    <f a>4
    
    % bar 25
    <f a>2
    d,4

    % bar 26
    d( f a)

    % bar 27
    a2.~

    % bar 28
    a4
    fis( g)

    % bar 29
    e'2. ~

    % bar 30
    e4
    \slurUp
    c( e,)
    \slurNeutral

    % bar 31
    e2( d4)

    % bar 32
    a'2( g4)

    % bar 33
    c,4.( c'8->) c4->

    % bar 34 (finish)
    c4-> r2 \bar "|."
  }
}

lower =  {
  \relative c {
    \time 3/4
    \key c \major
    \clef bass
    \partial 4

    r4
    r2.

    % bar 3 - 6
    c4 \p <e g> <e g>

    c <e g> <e g>
    c <e g> <e g>
    c <e g> <e g>

    % bar 7 - 14
    d <f g> <f g>
    d <f g> <f g>
    d <f g> <f g>
    d <f g> <f g>
    d <f g> <f g>
    d <f g> <f g>
    d <f g> <f g>
    d <f g> <f g>

    % bar 15 - 18
    c <e g> <e g>
    c <e g> <e g>
    c <e g> <e g>
    c <e g> <e g>

    % bar 19 - 22
    e <g c> <g c>
    e <g c> <g c>
    e <g c> <g c>
    e <g c> <g c>

    % bar 23 - 24
    f <a d> <a d>
    f <a d> <a d>

    % bar 25
    <f a d>2.

    % bar 26
    a4 f d

    % bar 27
    d <f g> <f g>

    % bar 28
    b, <f' g> <f g>

    % bar 29
    c <e g> <e g>

    % bar 30
    e <g c> <g c>

    % bar 31
    <f a c>2.

    % bar 32
    <d g b>2.

    % bar 33
    <c e g>2
    <c e g >4->

    % bar 34 (finish)
    <c e g >->
    r2
  }
}

% #(set-global-staff-size 24)
\score {
  \new PianoStaff <<
    \set PianoStaff.instrumentName = "PIANO  "
    \new StaffTwinNote = "up" {
      \staffSize 1
      \upper
    }
    \context Dynamics=dynamics \middleDynamics
    \new StaffTwinNote = "down" {
      \staffSize 1
      \lower
    }
  >>

  \layout {    }

  \midi {
    \tempo 4 = 150
    \context {
      \type "Performer_group"
      \name Dynamics
      \consists "Dynamic_performer"
    }
  }
}