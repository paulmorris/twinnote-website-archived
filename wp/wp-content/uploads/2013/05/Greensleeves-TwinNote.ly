\version "2.17.19"

\include "twinnote-scripts.ly"

#(set-default-paper-size "letter")
\pointAndClickOff

\header {
  title = "Greensleeves"
  composer = "Traditional; 16th Century English Melody"
  mutopiatitle = "Greensleeves (hymntune)"
  mutopiacomposer = "Traditional"
  mutopiainstrument = "Voice (SATB)"
  date = ""
  maintainer = "Steve Dunlop"
  maintainerWeb = "http://www.nerstrand.net"
  maintainerEmail = "music@nerstrand.net"
  style = "Hymn"
  source = "www.cyberhymnal.org"
  lastupdated = "2008/1/12"
  footer = "Mutopia-2008/01/13-1247"
  tagline = \markup { \override #'(box-padding . 1.0) \override #'(baseline-skip . 2.7) \box \center-column { \teeny \line {Sheet music typeset/transcribed into TwinNote music notation by Paul Morris using LilyPond. Details: \with-url #"http://twinnote.org" http://twinnote.org }  \line { \teeny \line { This work is licensed under a \with-url #"http://creativecommons.org/licenses/by-sa/3.0/" {Creative Commons Attribution-ShareAlike 3.0 Unported License.} } } \teeny \line { Public domain source file from \with-url #"http://www.MutopiaProject.org" \line { \teeny www. \hspace #-1.0 MutopiaProject \hspace #-1.0 \teeny .org \hspace #0.5 }  \hspace #0.5 \italic Free to download, with the \italic freedom to distribute, modify and perform. } \line { \teeny \line { Typeset using \with-url #"http://www.LilyPond.org" \line { \teeny www. \hspace #-1.0 LilyPond \hspace #-1.0 \teeny .org } by \maintainer. \hspace #-1.0 \hspace #0.5 Reference: \footer } }  } }
}

Soprano = {
  \time 6/8 \partial 8
  \key e \minor
  e'8 g'4 a'8 b'8. c''16 b'8
  a'4 fis'8 d'8. e'16 fis'8
  g'4 e'8 e'8. dis'16 e'8
  fis'4. b4 e'8

  g'4 a'8 b'8. c''16 b'8
  a'4 fis'8 d'8. e'16 fis'8
  g'8. fis'16 e'8 dis'8. cis'16 dis'8
  e'4. e'4 r8
  \break

  d''4. d''8. cis''16 b'8
  a'4 fis'8 d'8. e'16 fis'8
  g'4 e'8 e'8. dis'16 e'8
  fis'4 dis'8 b4 r8

  d''4. d''8. c''16 b'8
  a'4 fis'8 d'8. ( e'16 ) fis'8
  g'8. fis'16 e'8 dis'8. cis'16 dis'8
  e'4. e'4 s8  \bar "|."
}

Alto = {
  \key e \minor
  b8 e'4 d'8 d'8. c''16 g'8
  fis'4 d'8 d'8. e'16 c'8
  b4 b8 a8. dis'16 e'8
  dis'4. b4 b8

  e'4 d'8 d'8. c''16 g'8
  fis'4 d'8 d'8. e'16 c'8
  b8. fis'16 c'8 b8. cis'16 b8
  b4. b4 r8

  fis'4. b'8. a'16 g'8
  fis'4 d'8 d'8. e'16 c'8
  b4 b8 a8. dis'16 e'8
  dis'4 b8 b4 r8

  fis'4. b'8. a'16 g'8
  fis'4 d'8 d'4 c'8
  b8. fis'16 c'8 b8. cis'16 b8
  b4. b4 s8  \bar "|."
}

Tenor = {
  \key e \minor
  g8 b4 a8 g4 b8
  d' d' a fis4 a8
  g4 g8 a4 e8
  b4. b4 g8

  b4 a8 g4 b8
  d'4 a8 fis4 a8
  g4 a8 fis4 fis8
  g4. g4 r8

  b4. d'4 d'8
  d'4 a8 fis4 a8
  g4 g8 a4 e8
  b4 fis8 b4 r8

  b4. d'4 d'8
  d'4 a8 fis4 a8
  g4 a8 fis4 fis8
  g4. g4 s8  \bar "|."
}

Bass = {
  \key e \minor
  e8 e4 fis8 g4 g8
  d4 d8 d4 dis8
  e4 e8 c4 c8
  b,4. b,4 e8

  e4 fis8 g4 g8
  d d d d4 fis8
  e4 a,8 b,4 b,8
  e4. e4 r8

  b4. g4 g8
  d4 d8 d4 dis8
  e4 e8 c4 c8
  b,4 b,8 b,4 r8

  b4. g4 g8
  d d d d4 dis8
  e4 a,8 b,4 b,8
  e4. e4 s8  \bar "|."
}

\score {
  \new GrandStaff <<
    \new StaffTwinNote = upper <<
      \set Staff.printPartCombineTexts = ##f
      \partcombine
      { \accidentalStyle "modern-cautionary" \Soprano }
      { \Alto}
    >>
    \new StaffTwinNote = lower <<
      \set Staff.printPartCombineTexts = ##f
      \clef bass
      \partcombine
      { \accidentalStyle "modern-cautionary" \Tenor }
      { \Bass }
    >>
  >>
  \midi { }
  \layout {
    % indent = 0
    \context {
      \Score
      \remove "Bar_number_engraver"
    }
  }
}