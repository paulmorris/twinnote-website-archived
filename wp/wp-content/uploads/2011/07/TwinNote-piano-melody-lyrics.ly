\version "2.17" 

\include "twinnote-scripts.ly"  

#(set-default-paper-size "letter")

\header{
  title = "TwinNote Template with Piano Staff, Melody, Lyrics"
  tagline = \markup \teeny {TwinNote Music Notation  twinnote.org   Music engraving by LilyPond  www.lilypond.org}
}

melody = \relative c'' {
  \clef treble
  \key c \major
  \time 4/4

  a b c d
}

text = \lyricmode {
  Aaa Bee Cee Dee
}

upper = \relative c'' {
  \clef treble
  \key c \major
  \time 4/4

  a4 b c d
}

lower = \relative c {
  \clef bass
  \key c \major
  \time 4/4

  a2 c
}

\score {
  <<
    \new StaffTwinNote = "melody staff"
      \with { 
        % \remove "Accidental_engraver"
        % \remove "Key_engraver"
        % \staffSize #-2
      }
      { \new Voice = "mel" { \melody } }

    \new Lyrics \lyricsto mel \text
    \new PianoStaff <<

  	  \new StaffTwinNote = "upper"
        \with { 
          % \remove "Accidental_engraver"
          % \remove "Key_engraver"
          % \staffSize #-2
        }
        { \upper }

      \new StaffTwinNote = "lower" 
        \with { 
          % \remove "Accidental_engraver"
          % \remove "Key_engraver"
          % \staffSize #-2
        }
        { \lower }
    >>
  >>

  \layout {
    \context { \Staff \RemoveEmptyStaves }
  }
  \midi { }
}