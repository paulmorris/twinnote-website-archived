\version "2.17"  

\include "twinnote-scripts.ly"  

#(set-default-paper-size "letter")  

\header{
  title = "TwinNote Template with Single Staff"
  tagline = \markup \teeny {TwinNote Music Notation  twinnote.org   Music engraving by LilyPond  www.lilypond.org}
}

melody = \relative c' {
  \clef treble
  \key c \major
  \time 4/4

  a4 b c d
}

\score {
  \new StaffTwinNote
    \with { 
      % \remove "Accidental_engraver"
      % \remove "Key_engraver"
      % \staffSize #-2
    }
    { \melody }
  \layout { }
  \midi { }
}