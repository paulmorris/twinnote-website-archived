\version "2.17"  

\include "twinnote-scripts.ly"  

#(set-default-paper-size "letter")  

\header{
  title = "TwinNote Template with Single Staff and Lyrics"
  tagline = \markup \teeny {TwinNote Music Notation  twinnote.org   Music engraving by LilyPond  www.lilypond.org}
}

melody = \relative c' {
  \clef treble
  \key c \major
  \time 4/4

  a4 b c d
}

text = \lyricmode {
  Aaa Bee Cee Dee
}

\score {
  <<
    \new StaffTwinNote
      \with { 
        % \remove "Accidental_engraver"
        % \remove "Key_engraver"
        % \staffSize #-2
      }
      { \new Voice = "one" { \melody } }
    \new Lyrics \lyricsto "one" \text
  >>
  \layout { }
  \midi { }
}