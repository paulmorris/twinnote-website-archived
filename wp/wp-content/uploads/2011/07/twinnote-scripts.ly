\version "2.17.29"

% TWINNOTE SCRIPTS
% version: Oct 22, 2013

% Absolute value helper function
% when LilyPond goes to Guile 2.0 you can just use "abs" without defining it here
#(define (abs x) (if (> x 0) x (- 0 x)))

% PITCH LAYOUT ON STAFF
#(define (twinnote-pitch-layout p)
   (floor (/ (+ (ly:pitch-semitones p) 1) 2)))

% NOTE HEAD STENCILS
% trad oval dimensions: 
% horizontal: 0  0.65625  1.3125
% vertical: -0.5 0 0.5

#(define (make-notehead-stencil width isUp width-adjust)
   (let* ((width (* width width-adjust))
          (Xright width)
          (Xleft 0)
          (Xmid (* Xright 0.5))
          (Xcurv-right (+ Xright (* width 0.076))) ;; 0.076 of 1.3125 is ~ 0.1
          (Xcurv-left (- Xleft (* width 0.076)))
          (Ytop 0)
          (Ybottom -1)
          (Ycurv -0.5)
          (line-width 0.1)
          ;; nr = inner border for downwards triangle
          (Xright-nr (- Xright (* width 0.152))) ;; 0.152 of 1.3125 is ~ 0.2
          (Xleft-nr (+ Xleft (* width 0.152)))
          (Xmid-nr Xmid)
          (Xcurv-right-nr (+ Xright-nr (* width 0.053))) ;; 0.053 of 1.3125 is ~ 0.07
          (Xcurv-left-nr (- Xleft-nr (* width 0.053)))
          (Ytop-nr (- Ytop 0.2))
          (Ybottom-nr (+ Ybottom 0.08))
          (Ycurv-nr (- Ytop 0.5))
          ;; draw the stencil
          (stl (if isUp
                   ;; upwards triangle
                   (ly:stencil-rotate
                    (make-connected-path-stencil
                     `((,Xleft ,Ytop)
                       (,Xcurv-left ,Ycurv   ,Xmid ,Ybottom   ,Xmid ,Ybottom)
                       (,Xmid ,Ybottom   ,Xcurv-right ,Ycurv   ,Xright ,Ytop)
                       (,Xleft ,Ytop))
                     line-width   1.0   1.0   #f   #t)
                    180 0 0)
                   ;; downwards hollow triangle
                   (make-connected-path-stencil
                    `((,Xleft ,Ytop)
                      (,Xcurv-left ,Ycurv   ,Xmid ,Ybottom   ,Xmid ,Ybottom)
                      (,Xmid ,Ybottom   ,Xcurv-right ,Ycurv   ,Xright ,Ytop)
                      (,Xleft ,Ytop)
                      ;; inner border
                      (,Xleft-nr ,Ytop-nr)
                      (,Xright-nr ,Ytop-nr)
                      (,Xcurv-right-nr ,Ycurv-nr  ,Xmid-nr ,Ybottom-nr  ,Xmid-nr ,Ybottom-nr)
                      (,Xmid-nr ,Ybottom-nr ,Xcurv-left-nr ,Ycurv-nr ,Xleft-nr ,Ytop-nr))
                    line-width   1.0   1.0   #f   #t))))
     ;; move the stencil into place
     (ly:stencil-translate stl
       (cons 
        (* -0.5 (- width 1.3125))
        0.5))))

% NOTE HEAD HANDLER
%
% shapes, size, and stem attachment
%
% First get needed properties from the notehead grob:
% 1. resizing value (fsz --> mult)
% 2. pitch (ptch) --> pitch in semitones (semi)
% 3. note column (notecol, parent of notehead grob) --> stem (stm) --> stem direction (stmdir)
% 4. set stem attachment values
%
% Note Heads: assign correct notehad using pitch in semitones,
% resize based on current font-size using stencil-scale
%
% Stem Attachment: Based on semitone pitch (odd or even),
% is the notehead an up or down triangle,
% Then based on stem direction, assign stem attachment values

#(define ((twinnote-note-heads upTri dnTri upTri-wn dnTri-wn) grob)
   (let* ((fsz (ly:grob-property grob 'font-size 0.0))
          (mult (magstep fsz))
          (ptch (ly:event-property (event-cause grob) 'pitch))
          (semi (ly:pitch-semitones ptch))
          (dur-log (ly:grob-property grob 'duration-log))
          (notecol (ly:grob-parent grob X))
          (stm (ly:grob-object notecol 'stem))
          (stmdir (ly:grob-property stm 'direction))
          (upTriUpStem (cons 1 -0.98))
          (upTriDnStem (cons 1 0.80))
          (dnTriUpStem (cons 1 0.90))
          (dnTriDnStem (cons 1 -0.98)))
     ;; Note Heads
     (set! (ly:grob-property grob 'stencil)
           (ly:stencil-scale
            (if (= (remainder semi 2) 0)
                (if (< dur-log 1) dnTri-wn dnTri)
                (if (< dur-log 1) upTri-wn upTri))
            mult mult))
     ;; Stem Attachment
     (set! (ly:grob-property grob 'stem-attachment)
           (if (= (remainder (abs semi) 2) 1)
               (if (= UP stmdir) upTriUpStem upTriDnStem)
               (if (= DOWN stmdir) dnTriDnStem dnTriUpStem)))))


% HALF NOTE STEMS
%
% double-stems. Use -0.42 or 0.15 to change which side the 2nd stem appears
%
#(define (double-stemmer grob)
   (if (= 1 (ly:grob-property grob 'duration-log))
       (ly:stencil-combine-at-edge
        (ly:stem::print grob)
        X
        (- (ly:grob-property grob 'direction))
        (ly:stem::print grob)
        -0.42 )
       (ly:stem::print grob)))


% RESTS
%
%  fixes some vertical positioning
%
#(define (twinnote-rests grob)
   (let* (;; get duration of the rest
          (dur-log (ly:grob-property grob 'duration-log))
          (dot (ly:grob-object grob 'dot))
          (y-off (ly:grob-property grob 'Y-offset))
          ;; grob direction indicates polyphonic voices:
          ;; \voiceOne or \voiceThree is  +1
          ;; \voiceTwo or \voiceFour is -1
          ;; \oneVoice is 0 or a function?
          (grob-dir (ly:grob-property-data grob 'direction))
          (offset (if (or (equal? grob-dir 1) (equal? grob-dir -1))
                      0 ;; \voiceOne or \voiceTwo
                      1 ;; \oneVoice
                      )))
     ;; stencils, not used:
     ;; (dot-sil (if  (ly:grob? dot) (ly:grob-property dot 'stencil)))
     ;; (rest-sil (ly:grob-property grob 'stencil))
     
     ;; shift rests longer than a whole rest
     (cond
      ((< dur-log 0)
       (ly:grob-set-property! grob 'Y-offset
         (+ y-off offset))))
     
     ;; shift dots on rests, if they have one but not for: 
     ;; whole, half, 32nd, 64th, 128th rests (already correct)
     ;; and only when offset is 1 (default position in center of staff)
     (cond
      ((and (ly:grob? dot)
            (and (= offset 1)
                 (and (< dur-log 5)
                      (or (< dur-log 0) (> dur-log 1)))))
       (ly:grob-set-property! dot 'Y-offset 0.5)))))


% ACCIDENTALS
%
#(define (make-sharp-flat-stencil isSharp)
   (let*((ln (ly:stencil-translate
              (make-connected-path-stencil
               '((0  1.2)) 0.2 1 1 #f #f)
              (cons 0 -0.6)))
         (crcl (make-circle-stencil 0.24 0.01 #t)))
     (if isSharp
         ;; sharp
         (ly:stencil-add ln (ly:stencil-translate crcl (cons 0  0.5)))
         ;; flat
         (ly:stencil-add ln (ly:stencil-translate crcl (cons 0 -0.5))))))

#(define (make-double-sharp-flat-stencil isSharp)
   (ly:stencil-add
    (ly:stencil-translate
     (make-sharp-flat-stencil isSharp)
     (cons -0.25 0))
    (ly:stencil-translate
     (make-sharp-flat-stencil isSharp)
     (cons  0.25 0))))

% Helper function that changes and resizes the accidental stencils.
%
#(define (redraw-acc-sign grob acc mult)
   ;; set the 'X-extent and 'Y-extent
   (ly:grob-set-property! grob 'Y-extent (cons -0.5 1.2))
   (ly:grob-set-property! grob 'X-extent
     (cond
      ;; double flat or sharp
      ((or (= acc -1) (= acc 1)) (cons -0.64 0.37))
      ;; flat or sharp
      ((or (= acc -0.5) (= acc 0.5)) (cons -0.27 0.27))
      ;; natural
      ((= acc 0) (cons -0.0  (* 0.666666 0.65)))))
   ;; set the stencil
   (ly:grob-set-property! grob 'stencil
     (ly:stencil-scale
      (cond
       ((= acc -1) (make-double-sharp-flat-stencil #f))
       ((= acc -0.5) (make-sharp-flat-stencil #f))
       ((= acc 0) (ly:stencil-scale (ly:grob-property grob 'stencil) 0.65 0.65 ))
       ((= acc 0.5) (make-sharp-flat-stencil #t))
       ((= acc 1) (make-double-sharp-flat-stencil #t))
       (else (ly:grob-property grob 'stencil)))
      mult mult)))

% for storing the bar number and the accidentals in the current bar/measure so far
barnum = 0
acc-list = #'()

#(define TwinNote_accidental_engraver
   (make-engraver
    (acknowledgers
     ((accidental-interface engraver grob source-engraver)
      (let* ((mult (magstep (ly:grob-property grob 'font-size 0.0)))
             (acc (accidental-interface::calc-alteration grob))
             ;; get the current bar number
             (context (ly:translator-context engraver))
             (curr-barnum (ly:context-property context 'currentBarNumber))
             ;; get the pitch of the note in semitones
             (note-head (ly:grob-parent grob Y))
             (ptch (ly:event-property (event-cause note-head) 'pitch))
             (semi (ly:pitch-semitones ptch))
             (semi-modulo (modulo semi 12)))
        
        ;; if we're in a new measure, clear the acc-list, and set the barnum
        (cond ((not (= barnum curr-barnum))
               (set! acc-list '())
               (set! barnum curr-barnum)))
        
        (cond
         ((equal? (cons semi-modulo acc) (assoc semi-modulo nats-semi-list))
          
          ;; IS NOT AN ACCIDENTAL
          ;; so see if it needs to revert one
          (cond
           ((and
             ;; semi is in the list but acc doesn't match
             (not (equal? #f (assoc semi acc-list)))
             (not (equal? acc (cdr (assoc semi acc-list)))))
            ;; revert an accidental, 
            ;; printo a sign and remove accidental from the acc-list
            ;; (ly:grob-set-property! grob 'stencil (make-circle-stencil 0.3 0.2 #t))
            (redraw-acc-sign grob acc mult)
            (set! acc-list (assoc-remove! acc-list semi)))
           
           (else
            ;; no reversion of accidental, print no sign
            (ly:grob-set-property! grob 'stencil #f)
            (ly:grob-set-property! grob 'X-extent '(0 . 0))
            (ly:grob-set-property! grob 'Y-extent '(0 . 0)))))
         
         ;; IS AN ACCIDENTAL
         (else
          (cond
           ((equal? (cons semi acc) (assoc semi acc-list))
            ;; not a new accidental in this measure, don't print sign
            (ly:grob-set-property! grob 'stencil #f)
            (ly:grob-set-property! grob 'X-extent '(0 . 0))
            (ly:grob-set-property! grob 'Y-extent '(0 . 0)))
           
           (else
            ;; new accidental in this measure
            ;; print sign and add to list
            (redraw-acc-sign grob acc mult)
            
            ;; first remove previous accidental with same semitone
            (if (not (equal? #f (assoc semi acc-list)))
                (set! acc-list (assoc-remove! acc-list semi)))
            (set! acc-list (acons semi acc acc-list)))))))))))


% NATS-SEMI-LIST
%
% nats-semi-list is created by the TwinNote_key_signature_engraver
% and used by the TwinNote_accidental_engraver
% it is a list of naturals in the current key and if they are sharp, flat, natural
% default is C major / A minor
%
nats-semi-list = #'((0 . 0) (2 . 0) (4 . 0) (5 . 0) (7 . 0) (9 . 0) (11 . 0))

% Helper function to add naturals to list of sharps or flats in the key sig.
% Returns a list of all natural notes in this key, 0 - 6, and whether they are
% sharp flat or natural.  Example output for D major:
% ((6 . 0) (5 . 0) (4 . 0) (2 . 0) (1 . 0) (0 . 1/2) (3 . 1/2))
%
#(define (make-nats-list key-alt-list)
   (let* ((n 0))
     ;; add naturals for any notes that don't have sharps or flats
     (while (< n 7)
       (if (equal? #f (assoc n key-alt-list))
           (set! key-alt-list (acons n 0 key-alt-list)))
       (set! n (+ n 1)))
     ;; return:
     key-alt-list))

% Helper function that converts diatonic naturals list to semitones
% Example output for D major:
% ((11 . 0) (9 . 0) (7 . 0) (6 . 1/2) (4 . 0) (2 . 0) (1 . 1/2))
%
#(define (make-nats-semi-list nats-list)
   (let* ((n 0)
          (acc 0)
          (semi 0)
          (to-semi '((0 . 0) (1 . 2) (2 . 4) (3 . 5) (4 . 7) (5 . 9) (6 . 11))))
     (set! nats-semi-list '())
     (while (< n 7)
       (set! acc (cdr (assoc n nats-list)))
       (set! semi (cdr (assoc n to-semi)))
       
       ;; apply accidentals to semitone values
       (set! semi (+ semi (* 2 acc)))
       
       ;; wrap-around with modulo (only 0-11)
       (if (or (> semi 11) (< semi 0))
           (set! semi (modulo semi 12)))
       
       (set! nats-semi-list (acons semi acc nats-semi-list))
       (set! n (+ n 1)))))


% KEY SIGNATURES ENGRAVER
%
% associative list for storing key signature stencils
key-stil-bank = #'()

%    acc-list: list of accidentals,
%    nats-list: list of naturals
%    acc-count: the number of accidentals in the key signature
%    key-acc-type: the accidental sign type, 1/2=sharp, -1/2=flat
%    tonic-num: number of the tonic note 0-6, C=0, B=6
%    tonic-acc: #f if the tonic note is not sharp or flat, otherwise a pair
%    maj-num: number of the tonic note 0-6, if the key sig were major
%    sig-type: position of black or white notes: 0=starts with white, 1=starts with black
%    mode-num: number of the mode 0-6
%    tonic-psn: vertical position of the tonic/mode indicator within the key sig
%    tonic-lr: whether the tonic indicator is left or right of the sig, -1.2=left 1.2=right
%
#(define TwinNote_key_signature_engraver
   (make-engraver
    (acknowledgers
     ((key-signature-interface engraver grob source-engraver)
      (let* ((grob-name (assq-ref (ly:grob-property grob 'meta) 'name))
             (context (ly:translator-context engraver))
             (tonic-pitch (ly:context-property context 'tonic))
             (tonic-num (ly:pitch-notename tonic-pitch))
             (acc-list (ly:grob-property grob 'alteration-alist))
             (acc-count (length acc-list))
             (nats-list '())
             (key-acc-type 0)
             (sig #f)
             (key-id "0-0-0")
             (mult (magstep (ly:grob-property grob 'font-size 0.0))))
        
        ;; set (nats-semi-list) for use by the accidental engraver
        (cond ((not (equal? 'KeyCancellation grob-name))
               (set! nats-list (make-nats-list acc-list))
               (make-nats-semi-list nats-list)))
        
        ;; get key type: sharp (1/2), flat (-1/2), or natural (0)
        (if (null? acc-list)
            (set! key-acc-type  0)
            (set! key-acc-type (cdr (list-ref acc-list 0))))
        
        ;; create a unique key id (string) for key-stil-bank
        (set! key-id
              (string-append
               (number->string tonic-num) "_"
               (number->string acc-count) "_"
               (number->string key-acc-type)))
        
        ;; set the key signature stencil
        (cond
         ;; print nothing for key cancellations
         ((equal? 'KeyCancellation grob-name)
          (ly:grob-set-property! grob 'stencil #f)
          (set! sig #f))
         ;; new sig, create stil and add it to key-stil-bank
         ((equal? #f (assoc key-id key-stil-bank))
          (set! sig (draw-key-stencil grob key-acc-type acc-count tonic-num))
          (set! key-stil-bank (acons key-id sig key-stil-bank)))
         ;; else already in key-stil-bank, so use it
         (else
          (set! sig (cdr (assoc key-id key-stil-bank)))))
        
        ;; resize stil per current font size and set it
        (if (ly:stencil? sig)
            (ly:grob-set-property! grob 'stencil (ly:stencil-scale sig mult mult))))))))


% DRAW KEY SIGNATURE STENCIL
%
#(define (draw-key-stencil grob key-acc-type acc-count tonic-num)
   (let* ((maj-num 0)
          (mode-num 0)
          (sig #f)
          (sig-type 1)
          (sigldgr #f)
          (vert-adj 0)
          (tonic-psn 0)
          (tonic-lr -0.9)
          (is-tonic-black #f)
          (top-line
           (ly:stencil-translate
            (make-connected-path-stencil
             '((0.01 0)) 0.05 1 1 #f #f)
            (cons 0.49 2)))
          (bble
           (ly:stencil-translate
            (ly:stencil-scale
             (grob-interpret-markup grob
               (markup (number->string acc-count)))
             0.6 0.6)
            (cons 0 -0.4)))
          (lldgr (make-connected-path-stencil
                  '((-0.7 0) (1 0)) 0.15 1 1 #f #f))
          (rldgr (make-connected-path-stencil
                  '((1.7 0) (0 0)) 0.15 1 1 #f #f))
          (wldgr (make-connected-path-stencil
                  '((-0.7 0) (1.7 0)) 0.15 1 1 #f #f)))
     
     ;; create bble (that floats at top of key sig)
     (set! bble
           (ly:stencil-combine-at-edge
            (cond
             ;; sharp key
             ((= key-acc-type 1/2) (ly:stencil-translate (make-sharp-flat-stencil #t) (cons -0.5 0)))
             ;; flat key
             ((= key-acc-type -1/2) (ly:stencil-translate (make-sharp-flat-stencil #f) (cons -0.5 0)))
             ;; all natural key
             ((= key-acc-type 0) (ly:stencil-translate
                                  (ly:stencil-scale (grob-interpret-markup grob (markup #:natural)) 0.65 0.65)
                                  (cons -0.6 0.1))))
            0 1 bble 0.4))
     
     ;; settings based on key-acc-type and acc-count
     (cond
      ((= key-acc-type 0)
       (set! maj-num 0) (set! vert-adj -3.5) (set! sig-type 0) (set! sigldgr rldgr))
      ((= key-acc-type 1/2)
       (cond
        ((= acc-count 1) (set! maj-num 4) (set! vert-adj -1.5) (set! sigldgr wldgr))
        ((= acc-count 2) (set! maj-num 1) (set! vert-adj -3.0) (set! sig-type 0))
        ((= acc-count 3) (set! maj-num 5) (set! vert-adj -1.0))
        ((= acc-count 4) (set! maj-num 2) (set! vert-adj -2.5) (set! sig-type 0))
        ((= acc-count 5) (set! maj-num 6) (set! vert-adj -0.5) (set! sigldgr lldgr))
        ((= acc-count 6) (set! maj-num 3) (set! vert-adj -2.0) (set! sig-type 0) (set! sigldgr rldgr))
        ((= acc-count 7) (set! maj-num 0) (set! vert-adj -3.0) (set! sigldgr rldgr))))
      ((= key-acc-type -1/2)
       (cond
        ((= acc-count 1) (set! maj-num 3) (set! vert-adj -2.0))
        ((= acc-count 2) (set! maj-num 6) (set! vert-adj -1.0) (set! sig-type 0))
        ((= acc-count 3) (set! maj-num 2) (set! vert-adj -2.5))
        ((= acc-count 4) (set! maj-num 5) (set! vert-adj -1.5) (set! sig-type 0) (set! sigldgr lldgr))
        ((= acc-count 5) (set! maj-num 1) (set! vert-adj -3.0) (set! sigldgr rldgr))
        ((= acc-count 6) (set! maj-num 4) (set! vert-adj -2.0) (set! sig-type 0) (set! sigldgr rldgr))
        ((= acc-count 7) (set! maj-num 0) (set! vert-adj -0.5) (set! sigldgr lldgr)))))
     
     ;; calculate the mode number
     (set! mode-num (modulo (- tonic-num maj-num) 7))
     
     ;; set the vertical position coordinates for the tonic indicator "dot"
     (cond
      ((= mode-num 0) (set! tonic-psn -1))   ;; ionian, major
      ((= mode-num 1) (set! tonic-psn -1.5)) ;; dorian
      ((= mode-num 2) (set! tonic-psn -2))   ;; phrygian
      ((= mode-num 3) (set! tonic-psn -2.5)) ;; lydian
      ((= mode-num 4) (set! tonic-psn -3))   ;; mixolydian
      ((= mode-num 5) (set! tonic-psn -3.5)) ;; aeolian, minor
      ((= mode-num 6) (set! tonic-psn -4)))   ;; locrian
     
     (cond
      ((> mode-num 2)
       (set! tonic-lr 1.0)
       (cond
        ((= sig-type 1)
         (set! tonic-psn (+ 0.5 tonic-psn))))))
     
     ;; assemble two vertical bars based on sig-type
     (cond
      ((= sig-type 0)
       (set! sig (ly:stencil-add
                  ;; white left
                  (make-connected-path-stencil
                   '((0.5 0) (0.5 2) (0 2) (0 0)) 0.2 1 1 #f #f)
                  ;; black right
                  (ly:stencil-translate
                   (make-connected-path-stencil
                    '((0.5  0) (0.5  2.5) (0  2.5) (0  0) (0.5 0)) 0.01 1 1 #f #t)
                   (cons 0.5 1.5)))))
      ((= sig-type 1)
       (set! sig (ly:stencil-add
                  ;; black left
                  (make-connected-path-stencil
                   '((0.5 0) (0.5 2) (0 2) (0 0)) 0.001 1 1 #f #t)
                  ;; white right
                  (ly:stencil-translate
                   (make-connected-path-stencil
                    '((0.5 0) (0.5 2.5) (0 2.5) (0 0) (0.5 0)) 0.2 1 1 #f #f)
                   (cons 0.5 1.0))))))
     
     ;; prepare tonic indicator
     (cond
      ;; black tonic on left
      ((and (< mode-num 3) (= sig-type 1))
       (set! is-tonic-black #t)
       (set! tonic-lr (+ 0.08 tonic-lr)))
      ;; black tonic on right
      ((and (> mode-num 2) (= sig-type 0))
       (set! is-tonic-black #t)
       (set! tonic-lr (+ 0.03 tonic-lr))
       (set! tonic-psn (+ -0.05 tonic-psn)))
      ;; white tonic on left
      ((and (< mode-num 3) (= sig-type 0))
       (set! is-tonic-black #f)
       (set! tonic-lr (+ 0.00 tonic-lr)))
      ;; white tonic on right
      ((and (> mode-num 2) (= sig-type 1))
       (set! is-tonic-black #f)
       (set! tonic-lr (+ 0.12 tonic-lr))
       (set! tonic-psn (+ 0.05 tonic-psn))))
     
     ;; add tonic indicator to sig
     (set! sig (ly:stencil-combine-at-edge sig 1 -1
                 ;; tonic indicator sig
                 (ly:stencil-translate
                  (ly:stencil-scale
                   (make-notehead-stencil 1.3125 is-tonic-black 1)
                   0.6 1)
                  (cons tonic-lr 0))
                 (+ -0.1 tonic-psn)))
     
     ;; start with top-line marker and add sigldgr line
     (set! top-line (ly:stencil-combine-at-edge top-line 1 1 sigldgr -2.1))
     
     ;; add extra sigldgr line for Cmaj/Amin sig
     (cond ((= key-acc-type 0)
            (set! top-line (ly:stencil-combine-at-edge top-line 1 1 lldgr -5.1 ))))
     
     ;; position the sig
     (set! sig (ly:stencil-translate sig (cons 0 vert-adj)))
     
     ;; add the sig
     (set! sig (ly:stencil-add sig top-line))
     
     ;; add the acc type and count (bble) to top
     (cond
      ((= key-acc-type 0)
       (set! sig (ly:stencil-combine-at-edge (ly:stencil-translate sig (cons -0.4 0)) 1  1 bble 0.2)))
      (else
       (set! sig (ly:stencil-combine-at-edge (ly:stencil-translate sig (cons -0.4 0)) 1  1 bble 0.6))))
     
     ;; shift the whole sig to the right for proper spacing with clef
     (cond ((> mode-num 2)
            (set! sig (ly:stencil-translate sig (cons 0.35 0))))
       (else (set! sig (ly:stencil-translate sig (cons 0.9 0)))))
     
     ;; return the sig stencil
     sig ))


% CLEFS
%
#(define TwinNote_clef_engraver
   (make-engraver
    (acknowledgers
     ((clef-interface engraver grob source-engraver)
      (let* (
             (glyph-name (ly:grob-property grob 'glyph-name))
             (mult (magstep (ly:grob-property grob 'font-size 0.0)))
             (clef-minus-one (markup #:bold #:magnify 0.63 "-1"))
             (clef-minus-two (markup #:bold #:magnify 0.63 "-2")))
        ;; (newline)(display glyph-name)
        
        (set! (ly:grob-property grob 'stencil)
              (cond
               ;; G / Treble clef
               ((equal? glyph-name "clefs.G")
                (ly:font-get-glyph (ly:grob-default-font grob) "clefs.G_change"))
               ;; F / Bass clef
               ((or
                 (equal? glyph-name "clefs.F_change")
                 (equal? glyph-name "clefs.F"))
                (ly:stencil-combine-at-edge
                 (ly:stencil-translate (ly:font-get-glyph (ly:grob-default-font grob) "clefs.G_change") (cons 0 -2))
                 1 -1
                 (ly:stencil-translate (grob-interpret-markup grob clef-minus-two) (cons 0.2 0))
                 0.08))
               ;; C / Alto etc clef
               ((or
                 (equal? glyph-name "clefs.C_change")
                 (equal? glyph-name "clefs.C"))
                (ly:stencil-combine-at-edge
                 (ly:stencil-translate (ly:font-get-glyph (ly:grob-default-font grob) "clefs.G_change") (cons 0 -1))
                 1 -1
                 (ly:stencil-translate (grob-interpret-markup grob clef-minus-one) (cons 0.2 0))
                 0.08))
               (else
                (ly:grob-property grob 'stencil)))))))))

%{
  %  for larger, regular size clefs, if we ever get them working
  ;;  ((equal? glyph-name "clefs.F")
  ;;  (ly:stencil-combine-at-edge
  ;;    (ly:stencil-translate (ly:font-get-glyph (ly:grob-default-font grob) "clefs.G") (cons 0 -2))
  ;;    1 -1
  ;;    (ly:stencil-translate (grob-interpret-markup grob clef-fifteen) (cons 0.5 0))
  ;;    0.005 ))
%}


% CHORDS - AUTO
%
% automatically place note heads in chords/harmonies
% on the correct side of stem
%
#(define (twinnote-chord-handler grob)
   (let* ((heads-array (ly:grob-object grob 'note-heads))
          (note-heads
           (if (ly:grob-array? heads-array)
               (ly:grob-array->list heads-array)
               ;; handles case of no note-heads in NoteColumn grob (rests) 
               (list 0)))
          (total-heads (length note-heads))
          (head-grob #f)
          (semi 0)
          (stmdir (ly:grob-property (ly:grob-object grob 'stem) 'direction)) ;; 1 is up, -1 is down
          (n 0)
          (k 0)
          (m 0)
          (semi-list '())
          (int-list '())
          (clust-list '())
          (clust-list-length 0)
          (clust-count 0)
          (new-stemsides '()) ;; new, desired note placements (-1 left, 1 right)
          (old-stemsides '()) ;; old, original, default note placements
          (offset-list '()))
     
     ;; (newline) (display (ly:grob-property grob 'X-extent))
     ;; (display "clust-list: ") (display clust-list) (newline)
     ;; (display note-heads)
     ;; (display "----------------------") (newline)
     
     ;; single notes don't need offsetting
     (cond ((> total-heads 1)
            
            ;; GET SEMITONES
            ;; iterate through each note of the chord and
            ;; put the semitone of each note in semi-list
            ;; along with its position as entered in the .ly input file
            (while (< n total-heads)
              (set! head-grob (list-ref note-heads n))
              (set! semi (ly:pitch-semitones (ly:event-property (event-cause head-grob) 'pitch)))
              (set! semi-list (append semi-list (list (list n semi ))))
              (set! n (+ n 1)))
            
            ;; (display "----------------------") (newline)
            ;; (display semi-list) (display " - semi-list") (newline)
            
            ;; sort semi-list by semitone, ascending
            (set! semi-list (sort! semi-list (lambda (a b) (< (list-ref a 1) (list-ref b 1) ))))
            
            ;; (display semi-list) (display " - semi-list sorted") (newline)
            
            ;; GET INTERVALS
            ;; calculate the intervals between the semitones and put them into int-list
            ;; int-list always starts with a 0 so that its length is the same as total-heads
            (set! n 0)
            (while (< n total-heads)
              (if (= n 0)
                  (set! int-list (append int-list (list 0)))
                  (set! int-list (append int-list (list
                                                   (- (list-ref (list-ref semi-list n) 1)
                                                     (list-ref (list-ref semi-list (- n 1)) 1))))))
              (set! n (+ n 1)))
            
            ;; (display int-list) (display " - int-list")  (newline)
            
            ;; if there are no 3-semitone intervals, then there is
            ;; no need to offset anything, since standard layout is fine.
            (cond
             ((not (equal? #f (memq 3 int-list)))
              
              ;; GET CLUSTER PATTERN
              ;; since int-list starts with 0, we want to start with n = 1 and
              ;; clust-list starts with a 1 for the first note
              (set! n 1)
              (set! clust-list (list 1))
              (while (< n total-heads)
                (set! k (- (length clust-list) 1))
                ;; (display "k: ") (display k) (newline)
                ;; (display "clust-list: ") (display clust-list) (newline)
                
                (if (> (list-ref int-list n) 3)
                    (set! clust-list (append clust-list (list 1)))
                    (list-set! clust-list k (+ 1 (list-ref clust-list k ))))
                (set! n (+ n 1)))
              
              ;; (display clust-list) (display " - clust-list") (newline)
              
              ;; CALCULATE DESIRED STEM-SIDE POSITIONS
              ;; n tracks iteration through clust-list
              ;; k tracks the side of the stem (1 or -1)
              ;; clust-count tracks where you are within a cluster
              ;; m tracks which note of the chord you're on
              
              (set! n 0)
              (set! clust-list-length (length clust-list))
              
              (while (< n clust-list-length)
                (set! clust-count (list-ref clust-list n))
                
                ;; if down-stem and odd-numbered cluster (or single note, since 1 = odd)
                ;; then first/lowest head is on right side of stem
                ;; else first/lowest head is on left side of stem
                (cond
                 ((and (= stmdir -1) (odd? clust-count))
                  (set! k 1))
                 (else
                  (set! k -1)))
                
                ;; iterate through the cluster and put every
                ;; other note on the opposite side of the stem
                (while (> clust-count 0)
                  (set! new-stemsides
                        (append new-stemsides (list (list (list-ref (list-ref semi-list m) 0) k))))
                  (set! k (* k -1))
                  (set! clust-count (- clust-count 1))
                  (set! m (+ 1 m)))
                (set! n (+ 1 n)))
              
              ;; (display "----------------------") (newline)
              ;; (display semi-list) (display " - semi-list") (newline)
              ;; (display semi-list) (display " - semi-list sorted") (newline)
              ;; (display int-list) (display " - int-list")  (newline)
              ;; (display clust-list) (display " - clust-list") (newline)
              ;; (display new-stemsides) (display " - new-stemsides") (newline)
              
              ;; sort new-stemsides by position notes were entered in .ly input file
              (set! new-stemsides (sort! new-stemsides (lambda (a b) (< (list-ref a 0) (list-ref b 0) ))))
              ;; (display new-stemsides) (display " - new-stemsides sorted") (newline)
              
              ;; GET OLD, DEFAULT STEM-SIDE POSITIONS
              (set! n 0)
              (while (< n total-heads)
                (set! head-grob (list-ref note-heads n))
                ;; use round to avoid floating point number errors
                (set! k (round (ly:grob-relative-coordinate head-grob grob 0)))
                (cond
                 ;; left of up-stem
                 ((and (= stmdir 1) (= k 0))
                  (set! old-stemsides (append old-stemsides (list -1))))
                 ;; right of up-stem     
                 ((and (= stmdir 1) (positive? k))
                  (set! old-stemsides (append old-stemsides (list 1))))
                 ;; right of down-stem
                 ((and (= stmdir -1) (= k 0))
                  (set! old-stemsides (append old-stemsides (list 1))))
                 ;; left of down-stem
                 ((and (= stmdir -1) (negative? k))
                  (set! old-stemsides (append old-stemsides (list -1)))))
                (set! n (+ n 1)))
              
              ;; (display old-stemsides) (display " - old-stemsides") (newline)
              
              ;; GENERATE OFFSETS
              ;; if old-stemside and new-stemside are the same, 0 is the offset
              ;; otherwise use -1 or 1 from new-stemsides
              
              (set! n 0)
              (while (< n total-heads)
                (if (= (list-ref old-stemsides n) (list-ref (list-ref new-stemsides n) 1))
                    (set! offset-list (append offset-list (list 0)))
                    (set! offset-list (append offset-list (list (list-ref (list-ref new-stemsides n) 1)))))
                (set! n (+ n 1)))
              
              ;; (display offset-list) (display " - offset-list") (newline)
              
              ;; CALL THE MANUAL OFFSET FUNCTION
              ;; (check for 1 or -1 in offset-list) no need to send ( 0 0 0 0 0 )  
              ;; if there are only zeros in offset-list, there is nothing to offset       
              
              (cond
               ((not (and
                      (equal? #f (memq 1 offset-list))
                      (equal? #f (memq -1 offset-list))))
                ((shift-noteheads offset-list) grob))))))))) % end check for any 3-semitone intervals, end check for single notes 


% CHORDS - MANUAL
%
% For chords and intervals, manually shift note heads to left or right of stem
%
#(define ((shift-noteheads offsets) grob)
   "Defines how NoteHeads should be moved according to the given list of offsets."
   (let* (
          ;; NoteHeads
          ;; Get the NoteHeads of the NoteColumn
          (note-heads (ly:grob-array->list (ly:grob-object grob 'note-heads)))
          ;; Get their durations
          (nh-duration-log
           (map
            (lambda (note-head-grobs)
              (ly:grob-property note-head-grobs 'duration-log))
            note-heads))
          ;; Get the stencils of the NoteHeads
          (nh-stencils
           (map
            (lambda (note-head-grobs)
              (ly:grob-property note-head-grobs 'stencil))
            note-heads))
          ;; Get their length in X-axis-direction
          (stencils-x-lengths
           (map
            (lambda (x)
              (let* ((stencil (ly:grob-property x 'stencil))
                     (stencil-X-exts (ly:stencil-extent stencil X))
                     (stencil-lengths (interval-length stencil-X-exts)))
                stencil-lengths))
            note-heads))
          ;; Stem
          (stem (ly:grob-object grob 'stem))
          (stem-thick (ly:grob-property stem 'thickness 1.3))
          (stem-x-width (/ stem-thick 10))
          (stem-dir (ly:grob-property stem 'direction))
          
          ;; stencil width method, doesn't work with non-default beams
          ;; so using thickness property above instead
          ;; (stem-stil (ly:grob-property stem 'stencil))
          ;; (stem-x-width (if (ly:stencil? stem-stil)
          ;;                 (interval-length (ly:stencil-extent stem-stil X))
          ;;                 ;; if no stem-stencil use 'thickness-property
          ;;                 (/ stem-thick 10)))
          
          ;; Calculate a value to compensate the stem-extension
          (stem-x-corr
           (map
            (lambda (q)
              ;; TODO better coding if (<= log 0)
              (cond ((and (= q 0) (= stem-dir 1))
                     (* -1 (+ 2  (* -4 stem-x-width))))
                ((and (< q 0) (= stem-dir 1))
                 (* -1 (+ 2  (* -1 stem-x-width))))
                ((< q 0)
                 (* 2 stem-x-width))
                (else (/ stem-x-width 2))))
            nh-duration-log)))
     
     ;; (display offsets) (display " - offsets") (newline)
     
     ;; Final Calculation for moving the NoteHeads   
     (for-each
      (lambda (nh nh-x-length off x-corr)
        (if (= off 0)
            #f
            (ly:grob-translate-axis! nh (* off (- nh-x-length x-corr)) X)))
      note-heads stencils-x-lengths offsets stem-x-corr)))


% shift note heads
snhs =
#(define-music-function (parser location offsets) (list?)
   " Moves the NoteHeads, using (shift-noteheads offsets) "
   #{
     \once \override NoteColumn.before-line-breaking = #(shift-noteheads offsets)
   #})


setOtherScriptParent =
#(define-music-function (parser location which-note-head)(integer?)
   "If the parent-NoteHead of a Script is moved, another parent from the 
                                  NoteColumn could be chosen.
The NoteHeads are numbered 1 2 3...  not 0 1 2... "
#{
  %% Let "staccato" be centered on NoteHead, if Stem 'direction is forced
  %% with \stemUp, \stemDown, \voiceOne, \voiceTwo etc
  \once \override Script.toward-stem-shift = #0
  
  \once \override Script.after-line-breaking =
  #(lambda (grob)
     (let* ((note-head (ly:grob-parent grob X))
            (note-column (ly:grob-parent note-head X))
            (note-heads-list
             (ly:grob-array->list
              (ly:grob-object note-column 'note-heads)))
            (count-note-heads (length note-heads-list)))
       (if (> which-note-head count-note-heads)
           (ly:warning "Can't find specified note-head - ignoring")
           (set! (ly:grob-parent grob X)
                 (list-ref note-heads-list (- which-note-head 1))))))
#})


adjustStem =
#(define-music-function (parser location val)(pair?)
   "Adjust 'stem-attachment via 
                                     adding multiples of the stem-width to the x-default (car val)
 and multiplying the y-default with (cdr val).
"
#{
  \once \override NoteHead.before-line-breaking =
  #(lambda (grob)
     (let* ((stem-at (ly:grob-property grob 'stem-attachment))
            (stem (ly:grob-object grob 'stem))
            (stem-x-width (interval-length (ly:grob-property stem 'X-extent))))
       (ly:grob-set-property!
        grob
        'stem-attachment
        (cons (+ (car stem-at) (* stem-x-width (car val))) (* (cdr val) (cdr stem-at)))
        )))
#})


% TIME SIGNATURE
% adjust vertical position, currently not needed
% TwinNoteTimeSignature =
% #(lambda (grob) (set! (ly:grob-property grob 'Y-offset) -1))


% STAFF SIZE
%
% helper macro to zoom staff size
staffSize =
#(define-music-function (parser location new-size) (number?)
   #{
     \set Staff.fontSize = #new-size
     \override Staff.StaffSymbol.staff-space = #(magstep new-size)
     \override Staff.StaffSymbol.thickness = #(magstep new-size)
   #})


% STAFF DEFINITION
%
\layout {
  \context {
    \Staff
    \name StaffTwinNote
    \alias Staff
    
    \consists \TwinNote_key_signature_engraver
    \consists \TwinNote_clef_engraver
    
    % every note gets an accidental, and then they are overridden as needed
    \accidentalStyle dodecaphonic
    \consists \TwinNote_accidental_engraver
    \override Accidental.horizontal-skylines = #'()
    \override Accidental.vertical-skylines = #'()
    
    printKeyCancellation = ##f
    staffLineLayoutFunction = #twinnote-pitch-layout
    \override NoteHead.before-line-breaking =
    #(twinnote-note-heads
      (make-notehead-stencil 1.3125 #t 1) ;; up triangle
      (make-notehead-stencil 1.3125 #f 1) ;; down triangle
      (make-notehead-stencil 1.768 #t 1) ;; up whole note 
      (make-notehead-stencil 1.768 #f 1)) % down whole note
    \override StaffSymbol.line-positions = #'( 4 2 -2 -4 )
    \override Stem.stencil = #double-stemmer
    \override NoteColumn.before-line-breaking = #twinnote-chord-handler
    \override Rest.before-line-breaking = #twinnote-rests
    \numericTimeSignature
    
    % no longer needed:
    % \override TimeSignature.before-line-breaking = \TwinNoteTimeSignature
  }
  \context { \Score         \accepts StaffTwinNote }
  \context { \ChoirStaff  \accepts StaffTwinNote }
  \context { \GrandStaff \accepts StaffTwinNote }
  \context { \PianoStaff  \accepts StaffTwinNote }
  \context { \StaffGroup \accepts StaffTwinNote }
}

\midi {
  \context {
    \Staff
    \name StaffTwinNote
    \alias Staff
  }
  \context { \Score         \accepts StaffTwinNote }
  \context { \ChoirStaff  \accepts StaffTwinNote }
  \context { \GrandStaff \accepts StaffTwinNote }
  \context { \PianoStaff  \accepts StaffTwinNote }
  \context { \StaffGroup \accepts StaffTwinNote }
}