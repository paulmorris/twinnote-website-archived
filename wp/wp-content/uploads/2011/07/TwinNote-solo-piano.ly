\version "2.17"  

\include "twinnote-scripts.ly"  

#(set-default-paper-size "letter")

\header{
  title = "TwinNote Template for Solo Piano"
  tagline = \markup \teeny {TwinNote Music Notation  twinnote.org   Music engraving by LilyPond  www.lilypond.org}
}

upper = \relative c'' {
  \clef treble
  \key c \major
  \time 4/4

  a4 b c d e f g a
}

lower = \relative c {
  \clef bass
  \key c \major
  \time 4/4

  a2 c e g
}

\score {
  \new PianoStaff 
  <<
	\set PianoStaff.instrumentName = #"Piano"
    \new StaffTwinNote = upper
      \with { 
        % \remove "Accidental_engraver"
        % \remove "Key_engraver"
        % \staffSize #-2
      }
	  { \upper }
    \new StaffTwinNote = lower
      \with { 
        % \remove "Accidental_engraver"
        % \remove "Key_engraver"
        % \staffSize #-2
      }
	  { \lower }
  >>

  \layout { }
  \midi { }
}