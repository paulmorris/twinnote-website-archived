\version "2.17.29"

% TWINNOTE EXTRAS
% version: Oct 22, 2013

% 1.3125 is standard oval note head width
% 1.0 is standard oval note head height
% horizontal: 0   0.65625   1.3125
% vertical: -0.5   0   0.5

% Note Head Style I
% straight triangles, wider than traditional ovals

#(define (make-notehead-stencil-I width isUp width-adjust)
   (let* ((width (* width width-adjust))
          (Xright width)
          (Xleft 0)
          (Xmid (* Xright 0.5))
          (Ytop 0)
          (Ybottom -1)
          (line-width 0.09)
          ;; inner border for downTri
          (Xright-nr (- Xright (* width 0.165))) ;; 0.165 * 1.6875 = 0.28
          (Xleft-nr (+ Xleft (* width 0.165)))
          (Xmid-nr Xmid)
          (Ytop-nr (- Ytop 0.16))
          (Ybottom-nr (+ Ybottom 0.07))
          (stl (if isUp
                   ;; upTri
                   (ly:stencil-rotate
                    (make-connected-path-stencil
                     `((,Xleft ,Ytop)
                       (,Xmid ,Ybottom)
                       (,Xright ,Ytop)
                       (,Xleft ,Ytop))
                     line-width  1  1  #f  #t)
                    180 0 0)
                   ;; downTri
                   (make-connected-path-stencil
                    `((,Xleft ,Ytop)
                      (,Xmid ,Ybottom)
                      (,Xright ,Ytop)
                      (,Xleft ,Ytop)
                      ;; inner border
                      (,Xleft-nr ,Ytop-nr)
                      (,Xright-nr ,Ytop-nr)
                      (,Xmid-nr ,Ybottom-nr)
                      (,Xleft-nr ,Ytop-nr))
                    line-width  1  1  #f  #t))))
     (ly:stencil-translate stl
       (cons
        (* -0.5 (- width 1.3125))
        0.5))))

% Note Head Style II
% rounded triangles

#(define (make-notehead-stencil-II width isUp width-adjust)
   (let* ((width (* width width-adjust))
          (Xright width)
          (Xleft 0)
          (Xmid (* Xright 0.5))
          (Xcurv-right (+ Xmid (* width 0.166))) ;; 0.227 is 0.166 of 1.36
          (Xcurv-left (- Xmid (* width 0.166)))
          (Ytop 0)
          (Ybottom -0.9)
          (Ycurv -0.09)
          (line-width 0.15)
          ;; inner border for downTri
          (Xright-nr (- Xright (* width 0.11)))
          (Xleft-nr (+ Xleft (* width 0.11)))
          (Xmid-nr Xmid)
          (Xcurv-right-nr (+ Xmid (* width 0.166)))
          (Xcurv-left-nr (- Xmid (* width 0.166)))
          (Ytop-nr (- Ytop 0.15))
          (Ybottom-nr (+ Ybottom 0.02))
          (Ycurv-nr (- Ycurv 0.06))
          (stl (if isUp
                   ;; upTri
                   (ly:stencil-rotate
                    (make-connected-path-stencil
                     `((,Xleft ,Ytop)
                       (,Xleft ,Ycurv   ,Xcurv-left ,Ybottom   ,Xmid ,Ybottom)
                       (,Xcurv-right ,Ybottom   ,Xright ,Ycurv   ,Xright ,Ytop)
                       (,Xleft ,Ytop))
                     line-width  1  1  #f  #t)
                    180 0 0)
                   ;; downTri
                   (make-connected-path-stencil
                    `((,Xleft ,Ytop)
                      (,Xleft ,Ycurv   ,Xcurv-left ,Ybottom   ,Xmid ,Ybottom)
                      (,Xcurv-right ,Ybottom   ,Xright ,Ycurv   ,Xright ,Ytop)
                      (,Xleft ,Ytop)
                      ;; inner border
                      (,Xleft-nr ,Ytop-nr)
                      (,Xright-nr ,Ytop-nr)
                      (,Xright-nr ,Ycurv-nr  ,Xcurv-right-nr ,Ybottom-nr  ,Xmid-nr ,Ybottom-nr)
                      (,Xcurv-left-nr ,Ybottom-nr ,Xleft-nr ,Ycurv-nr ,Xleft-nr ,Ytop-nr))
                    line-width  1  1  #f  #t))))
     (ly:stencil-translate stl
       (cons
        (* -0.5 (- width 1.3125))
        (* Ybottom -0.55)))))

% Note Head Style III
% "house" shapes, straight lines

#(define (make-notehead-stencil-III width isUp width-adjust)
   (let* ((width (* width width-adjust))
          (Xright width)
          (Xleft 0)
          (Xmid (* Xright 0.5))
          (Ytop 0)
          (Ybottom -1)
          (Ymid (+ (* Ybottom 0.5) 0.1))
          (line-width 0.1)
          ;; inner border for downTri
          (Xright-nr (- Xright (* width 0.12))) ;; 0.12 of 1.2625 is ~  0.15
          (Xleft-nr (+ Xleft (* width 0.12)))
          (Xmid-nr Xmid)
          (Ytop-nr (- Ytop 0.2))
          (Ybottom-nr (+ Ybottom 0.07))
          (Ymid-nr (+ (* Ybottom 0.5) 0.1))
          (stl (if isUp
                   ;; upTri
                   (ly:stencil-rotate
                    (make-connected-path-stencil
                     `((,Xleft ,Ytop)
                       (,Xleft ,Ymid)
                       (,Xmid ,Ybottom)
                       (,Xright ,Ymid)
                       (,Xright ,Ytop)
                       (,Xleft ,Ytop))
                     line-width   1.0   1.0   #f   #t)
                    180 0 0)
                   ;; downTri
                   (make-connected-path-stencil
                    `((,Xleft ,Ytop)
                      (,Xleft ,Ymid)
                      (,Xmid ,Ybottom)
                      (,Xright ,Ymid)
                      (,Xright ,Ytop)
                      (,Xleft ,Ytop)
                      ;; inner border
                      (,Xleft-nr ,Ytop-nr)
                      (,Xright-nr ,Ytop-nr)
                      (,Xright-nr ,Ymid-nr)
                      (,Xmid-nr ,Ybottom-nr)
                      (,Xleft-nr ,Ymid-nr)
                      (,Xleft-nr ,Ytop-nr))
                    line-width   1.0   1.0   #f   #t))))
     (ly:stencil-translate stl
       (cons
        (* -0.5 (- width 1.3125))
        0.55))))

% Note Head Style IV
% "house" shapes, slighty rounded points

#(define (make-notehead-stencil-IV width isUp width-adjust)
   (let* ((width (* width width-adjust))
          (Xright width)
          (Xleft 0)
          (Xmid (* Xright 0.5))
          (Xcurv-right (+ Xmid (* width 0.038))) ;; 0.038 of 1.3125 is ~ 0.05
          (Xcurv-left (- Xmid (* width 0.038)))
          (Ytop 0)
          (Ybottom -1)
          (Ymid (+ (* Ybottom 0.5) 0.08))
          (Ycurv (- Ymid 0.04))
          (line-width 0.1)
          ;; inner border for downTri
          (Xright-nr (- Xright (* width 0.12))) ;; 0.12 of 1.2625 is ~  0.15
          (Xleft-nr (+ Xleft (* width 0.12)))
          (Xmid-nr Xmid)
          (Xcurv-right-nr (+ Xmid (* width 0.038)))
          (Xcurv-left-nr (- Xmid (* width 0.038)))
          (Ytop-nr (- Ytop 0.2))
          (Ybottom-nr (+ Ybottom 0.08))
          (Ymid-nr (+ (* Ybottom 0.5) 0.1))
          (Ycurv-nr (- Ymid 0.05))
          (stl (if isUp
                   ;; upTri
                   (ly:stencil-rotate
                    (make-connected-path-stencil
                     `((,Xleft ,Ytop)
                       (,Xleft ,Ymid)
                       (,Xleft ,Ycurv ,Xcurv-left ,Ybottom ,Xmid ,Ybottom)
                       (,Xcurv-right ,Ybottom ,Xright ,Ycurv ,Xright ,Ymid)
                       (,Xright ,Ytop)
                       (,Xleft ,Ytop))
                     line-width   1.0   1.0   #f   #t)
                    180 0 0)
                   ;; downTri
                   (make-connected-path-stencil
                    `((,Xleft ,Ytop)
                      (,Xleft ,Ymid)
                      (,Xleft ,Ycurv ,Xcurv-left ,Ybottom ,Xmid ,Ybottom)
                      (,Xcurv-right ,Ybottom ,Xright ,Ycurv ,Xright ,Ymid)
                      (,Xright ,Ytop)
                      (,Xleft ,Ytop)
                      ;; inner border
                      (,Xleft-nr ,Ytop-nr)
                      (,Xright-nr ,Ytop-nr)
                      (,Xright-nr ,Ymid-nr)
                      (,Xright-nr ,Ycurv-nr ,Xcurv-right-nr ,Ybottom-nr   ,Xmid-nr ,Ybottom-nr)
                      (,Xcurv-left-nr ,Ybottom-nr  ,Xleft-nr ,Ycurv-nr  ,Xleft-nr ,Ymid-nr)
                      (,Xleft-nr ,Ytop-nr))
                    line-width   1.0   1.0   #f   #t))))
     (ly:stencil-translate stl
       (cons
        (* -0.5 (- width 1.3125))
        0.5))))

% function for changing note head type

NoteHeadStyle =
#(define-music-function (parser location type width-adjust) (string? number?)
   #{ \override NoteHead.before-line-breaking =
      #(cond
        ((string=? type "I")
         (twinnote-note-heads
          (make-notehead-stencil-I 1.6875 #t width-adjust)
          (make-notehead-stencil-I 1.6875 #f width-adjust)
          (make-notehead-stencil-I 2.2 #t width-adjust)
          (make-notehead-stencil-I 2.2 #f width-adjust)))
        ((string=? type "II")
         (twinnote-note-heads
          (make-notehead-stencil-II 1.36 #t width-adjust)
          (make-notehead-stencil-II 1.36 #f width-adjust)
          (make-notehead-stencil-II 1.768 #t width-adjust)
          (make-notehead-stencil-II 1.768 #f width-adjust)))
        ((string=? type "III")
         (twinnote-note-heads
          (make-notehead-stencil-III 1.2625 #t width-adjust)
          (make-notehead-stencil-III 1.2625 #f width-adjust)
          (make-notehead-stencil-III 1.768 #t width-adjust)
          (make-notehead-stencil-III 1.768 #f width-adjust)))
        ((string=? type "IV")
         (twinnote-note-heads
          (make-notehead-stencil-IV 1.3125 #t width-adjust)
          (make-notehead-stencil-IV 1.3125 #f width-adjust)
          (make-notehead-stencil-IV 1.768 #t width-adjust)
          (make-notehead-stencil-IV 1.768 #f width-adjust)))
        (else
         (twinnote-note-heads
          (make-notehead-stencil 1.3125 #t width-adjust) ;; up triangle
          (make-notehead-stencil 1.3125 #f width-adjust) ;; down triangle
          (make-notehead-stencil 1.768 #t width-adjust) ;; up whole note 
          (make-notehead-stencil 1.768 #f width-adjust)) ;; down whole note
         )) #})


% EXAMPLE USAGE
%{
\new StaffTwinNote \with
{
  \NoteHeadStyle IV 0.9
} {
  c d e f
}
%}