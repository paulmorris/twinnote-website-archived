\version "2.17"  

\include "twinnote-scripts.ly"  

#(set-default-paper-size "letter")

\header{
  title = "TwinNote Template with Piano Staff and Lyrics"
  tagline = \markup \teeny {TwinNote Music Notation  twinnote.org   Music engraving by LilyPond  www.lilypond.org}
}

upper = \relative c'' {
  \clef treble
  \key c \major
  \time 4/4

  a4 b c d
}

lower = \relative c {
  \clef bass
  \key c \major
  \time 4/4

  a2 c
}

text = \lyricmode {
  Aaa Bee Cee Dee
}

\score {
  \new GrandStaff <<
    \new StaffTwinNote = upper 
      \with { 
        % \remove "Accidental_engraver"
        % \remove "Key_engraver"
        % \staffSize #-2
      }
      { \new Voice = "singer" \upper }
    \new Lyrics \lyricsto "singer" \text
    \new StaffTwinNote = lower 
      \with { 
        % \remove "Accidental_engraver"
        % \remove "Key_engraver"
        % \staffSize #-2
      }
      { \lower }
  >>

  \layout {
    \context {
      \GrandStaff
      \accepts "Lyrics"
    }
    \context {
      \Lyrics
      \consists "Bar_engraver"
    }
  }
  \midi { }
}