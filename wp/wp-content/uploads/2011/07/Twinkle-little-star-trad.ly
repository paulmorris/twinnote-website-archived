\version "2.17.19"  

\include "twinnote-scripts.ly"  

#(set-default-paper-size "letter")
\pointAndClickOff

\header{
  title = "Twinkle Twinkle Little Star"
  tagline = \markup \teeny {TwinNote Music Notation | twinnote.org | Music engraving by LilyPond | www.lilypond.org}
}

upper = \relative c'' {
  \clef treble
  \key c \major
  \time 6/8

  c,4.\mf c 
  g' g
  a a
  g r8 r4
  \break
  
  f4. f 
  e e
  d d 
  c r8 r4
  \break
  
  g''4. g
  f f 
  e e 
  d r8 r4
  \break
  
  c8\p e g c, e g 
  c, f a c, f a
  c, e g c, e g
  d g b d, g b
  \break
  
  c,,4.\mf c 
  g' g
  a a
  g r8 r4
  
  f4. f 
  e e
  d d 
  c r8 r4
  
  r4 r8 c e g
  c4.\fermata r8 r4
}

lower = \relative c {
  \clef bass
  \key c \major
  \time 6/8

  c8\mf e g c, e g
  c, e g c, e g
  c, f a c, f a
  c, e g c, e g
  c, f a c, f a  
  c, e g c, e g  
  d g b d, g b
  c, e g g e c
  
  c e g c, e g
  c, f a c, f a
  c, e g c, e g
  d g b d, g b
  
  g,4.\f g
  f f
  e e 
  d r8 r4
  
  c'8\mf e g c, e g
  c, e g c, e g
  c, f a c, f a
  c, e g c, e g
  c, f a c, f a  
  c, e g c, e g  
  d g b d, g b
  c, e g c, e g
  
  c,\> e g r8 r4
  r4 r8 <c,, c'>4.\p \fermata
    
}

\score {
  <<  
    \new PianoStaff 
    <<
      \set PianoStaff.instrumentName = #"Piano"
      \new StaffTwinNote = upper
        \with { 
          \staffSize #1.6
        }
	{ \upper }
      \new StaffTwinNote = lower
        \with { 
          \staffSize #1.6
        }
	{ \lower }
    >>
    \new PianoStaff 
    <<
      \set PianoStaff.instrumentName = #"Piano"
      \new Staff = upperTMN
      { \upper }
      \new Staff = lowerTMN
      { \lower }
    >>
  >>

  \layout { }
  \midi { \tempo 4 = 130 }
}