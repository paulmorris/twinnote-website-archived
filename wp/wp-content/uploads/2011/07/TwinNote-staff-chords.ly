\version "2.17"  

\include "twinnote-scripts.ly"  

#(set-default-paper-size "letter")  

\header{
  title = "TwinNote Template with Single Staff and Chords"
  tagline = \markup \teeny {TwinNote Music Notation  twinnote.org   Music engraving by LilyPond  www.lilypond.org}
}

melody = \relative c' {
  \clef treble
  \key c \major
  \time 4/4

  f4 e8[ c] d4 g
  a2 ~ a
}

harmonies = \chordmode {
  c4:m f:min7 g:maj c:aug
  d2:dim b:sus
}

\score {
  <<
    \new ChordNames {
      \set chordChanges = ##t
      \harmonies
    }

    \new StaffTwinNote
      \with { 
        % \remove "Accidental_engraver"
        % \remove "Key_engraver"
        % \staffSize #-2
      }
      { \melody }
  >>
  \layout { }
  \midi { }
}