\version "2.15.32"

%%%% Begin TwinNote scripts 

%%%% Customize note head stencils based on pitch

upTriangle =
#(ly:make-stencil
    (list 'embedded-ps
    "gsave
      currentpoint translate
      newpath
      -.1875 -.5 moveto
      .65625 .5 lineto
      1.5 -.5 lineto
      closepath
      fill
      grestore" )
    (cons -.1875 1.5)
    (cons -.5 .5)
)

downTriangle =
#(ly:make-stencil
    (list 'embedded-ps
    "gsave
      currentpoint translate

      newpath
      .08 .34 moveto
      .65625 -.4 lineto
      1.2325 .34 lineto
      closepath
      0.12 setlinewidth
      stroke     

      newpath
      -.0775 .43 moveto
      .65625 -.43 lineto
      1.39 .43 lineto
      closepath
      0.1 setlinewidth
      stroke     
     
      newpath
      -.1675 .48 moveto
      .65625 -.48 lineto
      1.48 .48 lineto
      closepath
      0.04 setlinewidth
      stroke

      grestore" )
    (cons -.1875 1.5)
    (cons -.5 .5)
)

%Based on the semitone, assign correct note head
#(define (semitone-to-stencil semitone)
        (if (= (remainder semitone 2) 0) downTriangle upTriangle)
)

%Get the pitch in semitones from the note head grob
#(define (stencil-notehead grob)
   (semitone-to-stencil
     (ly:pitch-semitones (ly:event-property (event-cause grob) 'pitch))
   )
)


%%%% Attach stems to note heads correctly

%Stem attachment values as variables
upTriUpStem 		= #'(1 . -1)
upTridownStem 		= #'(1 . .9)
downTriUpStem		= #'(1 . .9)
downTriDownStem 	= #'(1 . -1)

%Based on pitch (odd or even), is the note head an up or down triangle, 
%Then based on stem direction, assign stem attachment values
#(define (pitch-to-stem pitch stemdir)
	(if (= (remainder (absolute-value (ly:pitch-semitones pitch) ) 2) 1) 
		(if (= UP stemdir) upTriUpStem upTridownStem)		(if (= DOWN stemdir) downTriDownStem downTriUpStem)
	)
)

%Get absolute value
#(define (absolute-value x) (if (> x 0) x (- 0 x)))

%Get the stem from notehead grob
#(define (notehead-get-notecolumn nhgrob)
   (ly:grob-parent nhgrob X))

#(define (notehead-get-stem nhgrob)
   (let ((notecolumn (notehead-get-notecolumn nhgrob)))
     (ly:grob-object notecolumn 'stem)))

%Get the pitch and stem direction from the grob
#(define (stem-adjuster nhgrob)
	(pitch-to-stem
	    (ly:event-property (event-cause nhgrob) 'pitch) 
		(ly:grob-property (notehead-get-stem nhgrob) 'direction) ))


%%%% Begin notehead shape and stem attachment tweaks

TwinNoteNoteHeads = 
#(lambda (grob) 
      (set! (ly:grob-property grob 'stencil) (stencil-notehead grob))
	  (set! (ly:grob-property grob 'stem-attachment) (stem-adjuster grob))
)


%%%% Half notes get double-stems

#(define (doubleStemmer grob)
   (if (= 1 (ly:grob-property grob 'duration-log))
		(ly:stencil-combine-at-edge
              (ly:stem::print grob)
              X
              (- (ly:grob-property grob 'direction))
              (ly:stem::print grob)
              -.42 ) ;; note: use .15 for other side
		(ly:stem::print grob)
	)
)


%%%% For chords/intervals, shift note heads to left or right of stem

#(define ((shift offsets) grob)
  (let ((note-heads (ly:grob-array->list (ly:grob-object grob 'note-heads))))
    (for-each
      (lambda (p q) 
			;; if the notehead is wider than 1.2, it is a triangle so offset it.
			(if (< 1.3
                  (+ 
                    (car (ly:grob-property p 'X-extent)) 
                    (cdr (ly:grob-property p 'X-extent))
                  ) 
                )
			    (set! (ly:grob-property p 'X-offset) (* q 1.45))
            )
      ) 
      note-heads offsets) 
  ) 
) 

shiftNHs =
#(define-music-function (parser location offsets) (list?)
  #{
    \once \override NoteColumn #'before-line-breaking = #(shift offsets)
  #}
)


%%%% Place pitches on the staff

TwinNotePitchLayout = 
#(lambda (p) (floor (/ (+ (ly:pitch-semitones p) 1) 2)))


%%%% Adjust Time Signature position

TwinNoteTimeSignature = 
#(lambda (grob) 
      (set! (ly:grob-property grob 'Y-offset) -.5)
)

%%%%% End TwinNote scripts


#(set-default-paper-size "letter")

 \header {
  title = "Für Elise"
  subtitle = "Clavierstuck in A Minor - WoO 59"
  composer = "Ludwig van Beethoven"
  mutopiatitle = "Für Elise"
  mutopiacomposer = "BeethovenLv"
  mutopiaopus = "WoO 59"
  mutopiainstrument = "Piano"
  date = "1810"
  source = "Breitkopf & Härtel"
  style = "Classical"
  maintainer = "Stelios Samelis"

 footer = "Mutopia-2009/08/31-931"
 tagline = \markup { \override #'(box-padding . 1.0) \override #'(baseline-skip . 2.7) \box \center-column { \teeny \line {Sheet music typeset/transcribed into TwinNote music notation by Paul Morris using LilyPond. Details: \with-url #"http://twinnote.org" http://twinnote.org } \teeny \line {Note: it does not include TwinNote's alternative clefs, accidental signs, or key signatures.} \line { \teeny \line { This work is licensed under a \with-url #"http://creativecommons.org/licenses/by-sa/3.0/" {Creative Commons Attribution-ShareAlike 3.0 Unported License.} } } \teeny \line { Public domain source file from \with-url #"http://www.MutopiaProject.org" \line { \teeny www. \hspace #-1.0 MutopiaProject \hspace #-1.0 \teeny .org \hspace #0.5 } • \hspace #0.5 \italic Free to download, with the \italic freedom to distribute, modify and perform. } \line { \teeny \line { Typeset using \with-url #"http://www.LilyPond.org" \line { \teeny www. \hspace #-1.0 LilyPond \hspace #-1.0 \teeny .org } by \maintainer. \hspace #-1.0 \hspace #0.5 Reference: \footer } }  } }
}

% set staff size, 20 is default, 22.45 and 25.2 work well with fonts
#(set-global-staff-size 23.45  )

\score {

 \new PianoStaff
 <<
 \new Staff = "up" \with {
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    staffLineLayoutFunction = #(lambda (p) (floor (/ (+ (ly:pitch-semitones p) 1) 2)))
    middleCPosition = #-6
    clefGlyph = #"clefs.G"
    clefPosition = #(+ -6 3.5)
    \override StaffSymbol #'line-positions = #'( 4 2 -2 -4 )
    \override NoteHead #'stencil = #stencil-notehead
    \override NoteHead #'stem-attachment = #stem-adjuster
    \override Stem #'stencil = #doubleStemmer
	\override TimeSignature #'before-line-breaking = \TwinNoteTimeSignature
  }
{
 \clef treble
 \key a \minor
 \time 3/8
 \override Score.MetronomeMark #'transparent = ##t
 \tempo 4 = 72
 \repeat volta 2 {
 \partial 8 e''16\pp^\markup { \bold "Poco moto." } dis'' e'' dis'' e'' b' d'' c'' a'8 r16 c' e' a' b'8 r16 e' gis' b'
 c''8 r16 e' e'' dis'' e'' dis'' e'' b' d'' c'' a'8 r16 c' e' a' b'8 r16 e' c'' b' }
 \alternative { { a'4 } { a'8 \bar "" r16 b' \partial 8 c''16 d'' } }
 \repeat volta 2 {
 e''8. g'16[ f'' e''] d''8. f'16[ e'' d''] c''8. e'16[ d'' c''] b'8 r16 e'[ e''] r r e''[ e'''] r r dis''
 e''8 r16 dis'' e'' dis'' e''16 dis'' e'' b' d'' c''
 a'8 r16 c' e' a' b'8 r16 e' gis' b' c''8 r16 e' e'' dis'' e'' dis'' e'' b' d'' c'' a'8 r16 c' e' a' b'8 r16 e' c'' b' }
 \alternative { { a'8 r16 b'[ c'' d''] } { a'8 r16 <e' c''>[ <f' c''> \shiftNHs #'(0 1 0) <e' g' c''>] } }

 \grace { f'16[ a'] } c''4 f''16. e''32 e''8([ d'']) bes''16. a''32 a''16( g'' f'' e'' d'' c'')
 bes'8[ a'] \appoggiatura bes'32 a'32[ g' a' bes'] c''4 d''16[ dis''] e''8. e''16[ f'' a'] c''4 d''16. b'32
 c''32[ g'' g' g''] a'[ g'' b' g''] c''[ g'' d'' g''] e''[ g'' c''' b''] a''[ g'' f'' e''] d''[ g'' f'' d'']
 c''32[ g'' g' g''] a'[ g'' b' g''] c''[ g'' d'' g''] e''[ g'' c''' b''] a''[ g'' f'' e''] d''[ g'' f'' d'']
 e''32[ f'' e'' dis''] e''[ b' e'' dis''] e''[ b' e'' dis''] e''8. b'16[ e'' dis'']
 e''8. b'16([ e'']) dis''( e'') dis''([ e'']) dis''([ e'']) dis''( e'') dis'' e'' b' d'' c''
 a'8 r16 c' e' a' b'8 r16 e' gis' b' c''8 r16 e' e'' dis'' e'' dis'' e'' b' d'' c'' a'8 r16 c' e' a' b'8 r16 e' c'' b'
 a'8 r16 b'16 c'' d'' e''8. g'16[ f'' e''] d''8. f'16[ e'' d''] c''8. e'16[ d'' c''] b'8 r16 e'[ e''] r
 r16 e''[ e'''] r r dis''( e'') r r dis''[ e'' dis''] e'' dis'' e'' b' d'' c''
 a'8 r16 c' e' a' b'8 r16 e' gis' b' c''8 r16 e' e'' dis'' e'' dis'' e'' b' d'' c'' a'8 r16 c' e' a' b'8 r16 e' c'' b'

 a'8 r r \shiftNHs #'(0 1 0 1) <e' g' bes' cis''>4. <f' a' d''>4 <cis'' e''>16[ \shiftNHs #'(-1 0) <d'' f''>] \shiftNHs #'(0 -1 0) <gis' d'' f''>4 \shiftNHs #'(0 -1 0) <gis' d'' f''>8 \shiftNHs #'(-1 0 0) <a' c''! e''>4.
 <f' d''>4 <e' c''>16[ <d' b'>] \shiftNHs #'(0 0 1) <c' fis' a'>4 <c' a'>8 <c' a'>8[ <e' c''> <d' b'>] <c' a'>4.
 \shiftNHs #'(0 1 0 1) <e' g' bes' cis''>4. <f' a' d''>4 <cis'' e''>16[ \shiftNHs #'(-1 0) <d'' f''>] \shiftNHs #'(-1 0) <d'' f''>4 \shiftNHs #'(-1 0) <d'' f''>8 \shiftNHs #'(-1 0) <d'' f''>4.
 <g' ees''>4 <f' d''>16[ <ees' c''>] \shiftNHs #'(0 1 0) <d' f' bes'>4 \shiftNHs #'(0 1 1) <d' f' a'>8 \shiftNHs #'(0 1 0) <d' f' gis'>4 \shiftNHs #'(0 1 0) <d' f' gis'>8 <c' e'! a'>4 r8 <e' b'>8 r r
 \set tupletSpannerDuration = #(ly:make-moment 1 8)
 \times 2/3 { a16\pp[ c' e'] a'[ c'' e''] d''[ c'' b'] a'[ c'' e''] a''[ c''' e'''] d'''[ c''' b''] \ottava #1
 a''[ c''' e'''] a'''[ c'''' e''''] d''''[ c'''' b'''] bes'''[ a''' gis'''] g'''[ \ottava #0 fis''' f'''] e'''[ dis''' d''']
 cis'''[ c''' b''] bes''[ a'' gis''] g''[ fis'' f''] }

 e''16 dis'' e'' b' d'' c'' a'8 r16 c' e' a' b'8 r16 e' gis' b'
 c''8 r16 e' e'' dis'' e'' dis'' e'' b' d'' c'' a'8 r16 c' e' a' b'8 r16 e' c'' b'
 a'8 r16 b'16 c'' d'' e''8. g'16[ f'' e''] d''8. f'16[ e'' d''] c''8. e'16[ d'' c''] b'8 r16 e'[ e''] r
 r16 e''[ e'''] r r dis''( e'') r r dis''[ e'' dis''] e'' dis'' e'' b' d'' c''
 a'8 r16 c' e' a' b'8 r16 e' gis' b' c''8 r16 e' e'' dis'' e'' dis'' e'' b' d'' c'' a'8 r16 c' e' a' b'8 r16 e' c'' b'
 a'8 r \bar "|."
}

 \new Staff = "down" \with {
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    staffLineLayoutFunction = #(lambda (p) (floor (/ (+ (ly:pitch-semitones p) 1) 2)))
    middleCPosition = #-6
    clefGlyph = #"clefs.G"
    clefPosition = #(+ -6 3.5)
    \override StaffSymbol #'line-positions = #'( 4 2 -2 -4 )
    \override NoteHead #'stencil = #stencil-notehead
    \override NoteHead #'stem-attachment = #stem-adjuster
    \override Stem #'stencil = #doubleStemmer
	\override TimeSignature #'before-line-breaking = \TwinNoteTimeSignature
  }
{
 \clef bass
 \key a \minor
 \time 3/8
 \repeat volta 2 {
 \partial 8 r8\pp R4. a,16\sustainOn e a r16 r8\sustainOff e,16\sustainOn e gis r r8\sustainOff
 a,16\sustainOn e a r r8\sustainOff R4. a,16\sustainOn e a r r8\sustainOff
 e,16\sustainOn e gis r r8\sustainOff }
 \alternative { { a,16 e a r } { a,16[ e \bar "" a16] r \partial 8 r8 } }
 \repeat volta 2 {
 c16\sustainOn g c' r r8\sustainOff g,16\sustainOn g b r r8\sustainOff
 a,16\sustainOn e a r r8 e,16 e e' r r \clef treble e'16[ e''] r r dis''[ e''] r\sustainOff r16 dis''[ e''] r r8 R4.
 \clef bass a,16\sustainOn e a r16 r8\sustainOff e,16\sustainOn e gis r r8\sustainOff
 a,16\sustainOn e a r r8\sustainOff R4. a,16\sustainOn e a r r8\sustainOff
 e,16\sustainOn e gis r r8\sustainOff }
 \alternative { { a,16 e a r r8 } { a,16[ e a] <bes c'>[ <a c'> <g bes c'>] } }

 f16 a c' a c' a f bes d' bes d' bes f e' <f g bes> e' <f g bes> e' f a c' a c' a f a c' a c' a e a c' a <d d'> f
 g16 e' g e' g f' \clef treble <c' e'>8 r16 <f' g'>[ \shiftNHs #'(0 1) <e' g'> \shiftNHs #'(0 1 0) <d' f' g'>] \shiftNHs #'(0 0 1) <c' e' g'>8 \clef bass <f a>8[ <g b>]
 \clef treble c'8 r16 <f' g'>[ \shiftNHs #'(0 1) <e' g'> \shiftNHs #'(0 1 0) <d' f' g'>] \shiftNHs #'(0 0 1) <c' e' g'>8 \clef bass <f a>8[ <g b>] \shiftNHs #'(-1 0) <gis b>8 r r R4.
 R4. R4. R4. a,16 e a r16 r8 e,16\sustainOn e gis r r8\sustainOff a,16\sustainOn e a r r8\sustainOff
 R4. a,16\sustainOn e a r r8\sustainOff e,16\sustainOn e gis r r8\sustainOff a,16 e a r r8
 c16\sustainOn g c' r r8\sustainOff g,16 g b r r8 a,16 e a r r8 e,16\sustainOn e e' r r
 \clef treble e'16([ e'']) r r dis''([ e'']) r\sustainOff r dis''([ e'']) r r8 R4.
 \clef bass a,16 e a r16 r8 e,16 e gis r r8 a,16 e a r r8 R4. a,16 e a r r8 e,16 e gis r r8

 a,16 a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a,
 <d, a,> <d, a,> <d, a,> <d, a,> <d, a,> <d, a,> <dis, a,> <dis, a,> <dis, a,> <dis, a,> <dis, a,> <dis, a,>
 <e, a,> <e, a,> <e, a,> <e, a,> <e, gis,> <e, gis,> <a,, a,> a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a,
 bes, bes, bes, bes, bes, bes, bes, bes, bes, bes, bes, bes, bes, bes, bes, bes, bes, bes,
 b,! b, b, b, b, b, c4 r8 <e gis>8 r r
 a,,8\sustainOn r <a c' e'> <a c' e'> r <a c' e'> <a c' e'> r <a c' e'> <a c' e'> r r R4.

 R4.\sustainOff a,16\sustainOn e a r r8\sustainOff e,16\sustainOn e gis r r8\sustainOff
 a,16\sustainOn e a r r8\sustainOff R4.
 a,16\sustainOn e a r r8\sustainOff e,16\sustainOn e gis r r8\sustainOff a,16 e a r r8
 c16\sustainOn g c' r r8\sustainOff g,16\sustainOn g b r r8\sustainOff
 a,16\sustainOn e a r r8\sustainOff e,16\sustainOn e( e') r r
 \clef treble e'16([ e'']) r r dis''([ e'']) r\sustainOff r dis''([ e'']) r r8 R4.
 \clef bass a,16\sustainOn e a r16 r8\sustainOff e,16\sustainOn e gis r r8\sustainOff a,16 e a r r8 R4.
 a,16\sustainOn e a r r8\sustainOff e,16\sustainOn e gis r r8\sustainOff <a,, a,>8 r \bar "|."
}
>>

 \layout { }

 \midi { }
}
